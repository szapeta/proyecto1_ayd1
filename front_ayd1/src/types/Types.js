export const types = {
    login: '[auth] login',
    logout: '[auth] logout',

    isProd: true,

    apiurl: 'http://localhost:3000/',
    bucketURL: 'https://localhost/',

    rolTurista: '[auth] UsuarioTurista',
    rolVehiculo: '[auth] UsuarioVehiculo',
    rolVuelo: '[auth] UsuarioVuelo',
    rolHotel: '[auth] UsuarioHotel',
    rolAdmin: '[auth] UsuarioAdmin',

    cte_Admin:0,
    cte_Turista:1,
    cte_Hotel:2,
    cte_Autos:3,
    cte_Vuelos:4,

    cte_strAdmin:0,
    cte_strTurista:'turista',
    cte_strHotel:'hotel',
    cte_strAutos:'auto',
    cte_strVuelos:'avion',
}