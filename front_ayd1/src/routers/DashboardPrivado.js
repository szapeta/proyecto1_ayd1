import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { HomeAdmin } from "../components/private/admin/homeadmin/HomeAdmin";
import { CrearHabitacion } from "../components/private/hotel/habitacionHotel/CrearHabitacion";
import { EditarHabitacion } from "../components/private/hotel/habitacionHotel/EditarHabitacion";
import { EliminarHabitacion } from "../components/private/hotel/habitacionHotel/EliminarHabitacion";
import { ConsultarReservacion } from "../components/private/hotel/reservacionHotel/ConsultarReservacion";
import { ReservarHotel } from "../components/private/turista/hotel/ReservarHotel";
import { ConfirmarHotel } from "../components/private/turista/hotel/ConfirmarReserva";
import { ConfirmarAuto } from "../components/private/turista/vehiculo/ConfirmarReserva";
import { VehiculoCarga } from "../components/private/vehiculo/VehiculoCarga";
import { VehiculoLst } from "../components/private/vehiculo/VehiculoLst";
import { VehiculoLstAlquiler } from "../components/private/vehiculo/VehiculoLstAlquiler";
import { VehiculoNew } from "../components/private/vehiculo/VehiculoNew";
import FooterAuth from "../components/rol/FooterAuth";
import { NavAuth } from "../components/rol/NavAuth";
import { ReservarVehiculo } from "../components/private/turista/vehiculo/ReservarVehiculo";
import { ReservarVuelo } from "../components/private/turista/vuelo/ReservarVuelo";
import { ConfirmarVuelo } from "../components/private/turista/vuelo/ConfirmarReserva";
import { CrearVuelo } from "../components/private/aerolineas/vuelo/CrearVuelo";
import { EliminarVuelo } from "../components/private/aerolineas/vuelo/EliminarVuelo";
import { ConsultarReservacionVuelo } from "../components/private/aerolineas/reservacionVuelo/ConsultarReservacion";

export const DashboardPrivado = () => {
  return (
    <>
      <NavAuth />
      <div className="container mt-2">
        <Switch>
          <Route exact path="/homeadmin" component={HomeAdmin} />
          <Route exact path="/crearhabitacion" component={CrearHabitacion} />
          <Route exact path="/editarhabitacion" component={EditarHabitacion} />
          <Route exact path="/eliminarhabitacion" component={EliminarHabitacion} />
          <Route exact path="/consultarreserva" component={ConsultarReservacion} />
          <Route exact path="/reservarhotel" component={ReservarHotel} />
          <Route exact path="/confirmarhotel" component={ConfirmarHotel} />
          <Route exact path="/vehiculoscreate" component={VehiculoNew} />
          <Route exact path="/reservarauto" component={ReservarVehiculo} />
          <Route exact path="/confirmarauto" component={ConfirmarAuto} />
          <Route exact path="/reservarvuelo" component={ReservarVuelo} />
          <Route exact path="/confirmarvuelo" component={ConfirmarVuelo} />
          <Route exact path="/crearvuelo" component={CrearVuelo} />
          <Route exact path="/eliminarvuelo" component={EliminarVuelo} />
          <Route exact path="/reservacionvuelos" component={ConsultarReservacionVuelo} />
          <Route exact path="/vehiculoscarga" component={VehiculoCarga} />
          <Route exact path="/vehiculosalquiler" component={VehiculoLstAlquiler} />
          <Route exact path="/vehiculos" component={VehiculoLst} />
          <Route exact path="/vehiculosadmnew" component={VehiculoNew} />
          

          <Redirect to="/homeadmin" />
        </Switch>
      </div>
      <FooterAuth />
    </>
  );
};
