
import React from 'react'
import { Link, NavLink, useHistory } from 'react-router-dom'

export const NavHoteles = () => {
  return (
    <>
            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link pr-5"
                    exact
                    to="/homehotel"
                >
                    Home
                </NavLink>
            </div>
            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link pr-5"
                    exact
                    to="/crearhabitacion"
                >
                    Crear habitación
                </NavLink>
            </div>
            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link pr-5"
                    exact
                    to="/eliminarhabitacion"
                >
                    Eliminar habitación
                </NavLink>
            </div>
            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link pr-5"
                    exact
                    to="/consultarreserva"
                >
                    Consultar reservas 
                </NavLink>
            </div>
        </>
  )
}
