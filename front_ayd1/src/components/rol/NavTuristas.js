import React from 'react'
import { Link, NavLink, useHistory } from 'react-router-dom'

export const NavTuristas = () => {
  return (
    <>
      <div className="navbar-nav">
        <NavLink
          activeClassName="active"
          className="nav-item nav-link pr-5"
          exact
          to="/hometurista"
        >
          Home
        </NavLink>
      </div>
      <div className="navbar-nav">
        <NavLink
          activeClassName="active"
          className="nav-item nav-link pr-5"
          exact
          to="/reservarhotel"
        >
          Hoteles
        </NavLink>
      </div>
      <div className="navbar-nav">
        <NavLink
          activeClassName="active"
          className="nav-item nav-link pr-5"
          exact
          to="/reservarvuelo"
        >
          Vuelos
        </NavLink>
      </div>
      <div className="navbar-nav">
        <NavLink
          activeClassName="active"
          className="nav-item nav-link pr-5"
          exact
          to="/reservarauto"
        >
          Automoviles
        </NavLink>
      </div>
    </>
  )
}
