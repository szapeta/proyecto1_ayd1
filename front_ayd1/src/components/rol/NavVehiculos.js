import React from 'react'
import { NavLink } from 'react-router-dom'

export const NavVehiculos = () => {
  return (
    <>
            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link pr-5"
                    exact
                    to="/vehiculos"
                >
                    Vehiculos
                </NavLink>
            </div>
            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link pr-5"
                    exact
                    to="/vehiculoscarga"
                >
                    Carga
                </NavLink>
            </div>
            <div className="navbar-nav pr-5">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link"
                    exact
                    to="/vehiculosalquiler"
                >
                    Alquileres
                </NavLink>
            </div>
        </>
  )
}
