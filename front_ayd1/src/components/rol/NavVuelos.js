import React from 'react'
import { NavLink } from 'react-router-dom'

export const NavVuelos = () => {
  return (
    <>
            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link pr-5"
                    exact
                    to="/crearvuelo"
                >
                    Crear vuelos
                </NavLink>
            </div>
            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link pr-5"
                    exact
                    to="/eliminarvuelo"
                >
                    Eliminar vuelo
                </NavLink>
            </div>
            <div className="navbar-nav pr-5">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link"
                    exact
                    to="/reservacionvuelos"
                >
                    Reservaciones
                </NavLink>
            </div>
        </>
  )
}
