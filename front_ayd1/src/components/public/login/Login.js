import axios from "axios";
import React, { useContext, useState } from "react";
import { AuthContext } from "../../../auth/AuthContext";
import { types } from "../../../types/Types";

export const Login = ({ history }) => {
    const { dispatch } = useContext(AuthContext);

    const [formusername, setFormusername] = useState("");
    const [formpass, setFormpass] = useState("");

    const handleLogin = async (e) => {

        const lastPath = localStorage.getItem("lastPath") || "/";

        let resp;
        if(types.isProd){
            resp = await axios.post(types.apiurl + "login", {
                user: formusername,
                pass: formpass,
            });
        }else{
            dispatch({
                type: types.login,
                payload: {
                    name: "sfelipe",
                    user: "sfelipe",
                    rol: types.rolVehiculo,
                    iduser: "1"                
                },
            });
            history.replace(lastPath);
            return
        }
    
        if (resp.data.length > 0) {

            let resultUsuario = resp.data[0];

            if(resultUsuario.msg==''){

            let valRol;
            if(resultUsuario.rol == types.cte_strAdmin){
                valRol = types.rolAdmin;
            }else if(resultUsuario.rol == types.cte_strTurista){
                valRol = types.rolTurista;
            }else if(resultUsuario.rol == types.cte_strHotel){
                valRol = types.rolHotel;
            }else if(resultUsuario.rol == types.cte_strAutos){
                valRol = types.rolVehiculo;
            }else if(resultUsuario.rol == types.cte_strVuelos){
                valRol = types.rolVuelo;
            }

                dispatch({
                    type: types.login,
                    payload: {
                        user:resultUsuario.iduser,
                        name: resultUsuario.fullname,
                        rol: valRol
                    },
                });
    
                history.replace(lastPath);
            }else{
                alert(resultUsuario.msg)
            }
            
        } else {
            alert("Error en credenciales");
        }
    };
    
    return (
        <div className="container mt-5">
            <h1>Login</h1>
            <hr />

            <div className="form-group">
                <label>Username</label>
                <input
                    type="ingrese su nombre de usuario"
                    autoComplete="off"
                    className="form-control"
                    id="name"
                    onChange={(event) => {
                        setFormusername(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Password</label>
                <input
                    type="password"
                    className="form-control"
                    id="pass"
                    onChange={(event) => {
                        setFormpass(event.target.value);
                    }}
                />
            </div>
            <button className="btn btn-outline-primary" onClick={handleLogin}>
                Login
            </button>
        </div>
    );
};
