import axios from "axios";
import "./Registro.css";
import React, { useContext, useState } from "react";
import { AuthContext } from "../../../auth/AuthContext";
import { types } from "../../../types/Types";

export const Registro = ({ history }) => {
    const { dispatch } = useContext(AuthContext);
    const [isError, setIsError] = useState("");
    const [frmrol, setFrmrol] = useState("");
    const [formname, setFormname] = useState("");
    const [fechanac, setFechanac] = useState("");
    const [formemail, setFormemail] = useState("");
    const [formpass, setFormpass] = useState("");
    const [formpassxtwo, setFormpassxtwo] = useState("");

    const lastPath = localStorage.getItem("lastPath") || "/";

    const convertToBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onload = () => {
                resolve(fileReader.result);
            };
            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    const handleRegistro = async (e) => {
        e.preventDefault();
        if (formpass == formpassxtwo) {
            setIsError("");
            let valRol;
            if(frmrol == types.cte_Admin){
                valRol = types.rolAdmin;
            }else if(frmrol == types.cte_Turista){
                valRol = types.rolTurista;
            }else if(frmrol == types.cte_Hotel){
                valRol = types.rolHotel;
            }else if(frmrol == types.cte_Autos){
                valRol = types.rolVehiculo;
            }else if(frmrol == types.ctc_Vuelos){
                valRol = types.rolVuelo;
            }

           await axios
                .post(types.apiurl + "newuser", {
                    name: formname,
                    fechanac: fechanac,
                    email: formemail,
                    password: formpass,
                    rol: frmrol
                })
                .then((resp) => {

                    dispatch({
                        type: types.login,
                        payload: {
                            name: formname,
                            user: resp.iduser,
                            rol: valRol
                        },
                    });

                    history.replace(lastPath);
                });
        }
    };

    return (
        <div className="container mt-5">
            <h1>Registro</h1>
            <hr />
            <form autoComplete="off">
                
                   
                        <div className="form-group">
                            <label>Nombre Completo</label>
                            <input
                                type="ingrese su nombre de usuario"
                                autoComplete="off"
                                className="form-control"
                                name="name"
                                id="name"
                                value={formname}
                                onChange={(event) => {
                                    setFormname(event.target.value);
                                }}
                            />
                        </div>
                        <div className="form-group">
                            <label>Fecha Nacimiento</label>
                            <input
                                type="date"
                                autoComplete="off"
                                className="form-control"
                                name="fechanac"
                                id="fechanac"
                                value={fechanac}
                                onChange={(event) => {
                                    setFechanac(event.target.value);
                                }}
                            />
                        </div>
                        <div className="form-group">
                            <label>Email</label>
                            <input
                                type="ingrese su nombre"
                                autoComplete="off"
                                className="form-control"
                                name="user"
                                id="user"
                                value={formemail}
                                onChange={(event) => {
                                    setFormemail(event.target.value);
                                }}
                            />
                        </div>
                        <div className="form-group">
                            <label>Rol:</label>
                            <select
                                id="idrol"
                                className="form-control"
                                onChange={(event) => {
                                    setFrmrol(event.target.value);
                                }}
                            >
                                <option value="" selected>
                                    - Elige un rol -
                                </option>
                                <option value="1">Turista</option>
                                <option value="2">Hotel</option>
                                <option value="3">Autos</option>
                                <option value="4">Vuelos</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input
                                type="password"
                                className="form-control"
                                id="pass"
                                value={formpass}
                                onChange={(event) => {
                                    setFormpass(event.target.value);
                                }}
                            />
                        </div>
                        <div className="form-group">
                            <label>Confirmar Password</label>
                            <input
                                value={formpassxtwo}
                                onChange={(event) => {
                                    setFormpassxtwo(event.target.value);
                                }}
                                type="password"
                                className="form-control"
                                id="passxtwo"
                            />
                        </div>
                        <button
                            className="btn btn-outline-primary"
                            onClick={handleRegistro}
                        >
                            Registrarse
                        </button>
                 
                
            </form>

            <div
                style={{
                    position: "absolute",
                    textAlign: "center",
                    paddingTop: 20,
                }}
            >
                {isError}
            </div>
        </div>
    );
};
