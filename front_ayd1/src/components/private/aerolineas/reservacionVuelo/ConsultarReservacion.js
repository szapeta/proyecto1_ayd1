import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";
import { ReservasLstItem } from './ReservasLstItem';

export const ConsultarReservacionVuelo = () => {
    const [listReservas, setListReservas] = useState([])
    const [fecha, setFecha] = useState("");  
    const [precio, setPrecio] = useState(0);  

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);
    
    const handleSearch = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `listareservasvuelos`,{
                fecha:fecha,
                precio: precio,
                id_service: user
            })
            .then((res) => {
                console.log("respuesta:", res);
                setListReservas(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };
    useEffect(() => {
        axios
            .post(types.apiurl + `listareservasvuelos`,{
                fecha:fecha,
                precio: precio,
                id_service: user
            })
            .then((res) => {
                console.log(res)
                setListReservas(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div>
        <div className=" pb-5 mb-5">
            <h1>Listado de reservas</h1>
            <hr />

            <div className="row">
                <div className="col-5">
                    <form onSubmit={handleSearch}>
                    <label>Precio:</label>
                        <input
                            type="text"
                            placeholder="precio"
                            className="form-control"
                            name="precio"
                            autoComplete="off"
                            value={precio}
                            onChange={(event) => {
                                setPrecio(event.target.value);
                            }}
                        />

                        <label>Fecha inicio:</label>
                        <input
                            type="date"
                            placeholder="fecha"
                            className="form-control"
                            name="fecha"
                            autoComplete="off"
                            value={fecha}
                            onChange={(event) => {
                                setFecha(event.target.value);
                            }}
                        />

                        <button
                            type="submit"
                            className="btn m-1 btn-block btn-outline-primary"
                        >
                            Buscar
                        </button>
                    </form>
                </div>
            </div>

            <table className="table">
                <thead>
                    <tr>
                        <th>Id vuelo</th>
                        <th>Fecha</th>
                        <th>Precio</th>
                    </tr>
                </thead>
                <tbody>
                {listReservas.map((Reserva) => (
                    <ReservasLstItem key={Reserva.id_vuelo} {...Reserva} />
                ))}
                </tbody>
            </table>
        </div>
    </div>
    );
};
