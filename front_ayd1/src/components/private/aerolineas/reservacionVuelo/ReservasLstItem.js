import React from "react";

export const ReservasLstItem = ({
    id_reserva_vuelo,
    asiento,
    usuario,
    id_vuelo
}) => {
    const handleDetalles = () => {};

    return (
        <tr>
            <td>{id_reserva_vuelo}</td>
            <td>{asiento}</td>
            <td>{usuario}</td>
        </tr>
    );
};
