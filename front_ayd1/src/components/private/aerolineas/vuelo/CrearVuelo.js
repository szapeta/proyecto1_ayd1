import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";

export const CrearVuelo = () => {
    const [Origen, setOrigen] = useState("");
    const [Destino, setDestino] = useState("");
    const [Precio, setPrecio] = useState(0);
    const [Fecha, setFecha] = useState("");
    const [cantidad_asiento, setCantAsiento] = useState(0);
    const [option, setOption] = useState(0);

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);

    const handleCrear = (e) => {
        e.preventDefault();
        console.log(user)
        axios
            .post(types.apiurl + `addVuelo`, {
                origen: Origen,
                precio: Precio,
                destino: Destino,
                fecha: Fecha,
                catnida_asiento: cantidad_asiento,
                vuelta: option,
                id_servicio: user
            })
            .then((res) => {
                console.log("respuesta:", res);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="container mt-5">
            <h2>Crear vuelo</h2>
            <hr />

            <div className="form-group">
                <label>Origen</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="origen"
                    onChange={(event) => {
                        setOrigen(event.target.value);
                    }}
                />
            </div>
            <div class="form-group">
                <label>Ida y vuelta:</label>
                <select class="form-control" onChange={(event) => {
                    setOption(event.target.selectedIndex);
                }}>
                    <option value="-1" selected>
                        -Elija una opcion:-
                    </option>
                    <option value="0" selected>
                        Ida
                    </option>
                    <option value="1" selected>
                        Ida y vuelta
                    </option>
                </select>
            </div>
            <div className="form-group">
                <label>Destino</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="destino"
                    onChange={(event) => {
                        setDestino(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Precio</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="precio"
                    onChange={(event) => {
                        setPrecio(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Cantidad asiento</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="cantAsiento"
                    onChange={(event) => {
                        setCantAsiento(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Fecha</label>
                <input
                    type="date"
                    autoComplete="off"
                    className="form-control"
                    id="fecha"
                    onChange={(event) => {
                        setFecha(event.target.value);
                    }}
                />
            </div>
            <button className="btn btn-outline-primary" onClick={handleCrear}>
                Crear habitación
            </button>
        </div>
    );
};
