import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";

export const EliminarVuelo = () => {
    const [idVuelo, setIdVuelo] = useState("");   

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);

    const handleEliminar = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `dropvuelo`, {
                id_vuelo: idVuelo
            })
            .then((res) => {
                console.log("respuesta:", res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="container mt-5">
            <h1>Eliminar vuelo</h1>
            <hr />
            <div className="form-group">
                <label>Id vuelo</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="id"
                    onChange={(event) => {
                        setIdVuelo(event.target.value);
                    }}
                />
            </div>
            <button className="btn btn-outline-primary" onClick={handleEliminar}>
                Eliminar vuelo
            </button>
        </div>
    );
};
