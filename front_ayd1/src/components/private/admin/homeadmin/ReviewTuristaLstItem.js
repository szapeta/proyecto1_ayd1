import React from "react";

export const ReviewTuristaLstItem = ({
    id_resena,
    comentario,
    id_servicio
}) => {
    return (
        <tr>
            <td>{id_resena}</td>
            <td>{comentario}</td>
            <td>{id_servicio}</td>
        </tr>
    );
};
