import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";
import { ReviewLstItem } from './ReviewLstItem';

export const ReviewList = () => {
    const [listComentarios, setListComentarios] = useState([])

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);

    useEffect(() => {
        axios
            .post(types.apiurl + `listcomentarios`, {
                user: user
            })
            .then((res) => {
                setListComentarios(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div>
        <div className=" pb-5 mb-5">
            <h3>Listado de comentarios</h3>
            <hr />
            <table className="table">
                <thead>
                    <tr>
                        <th>Id reseña</th>
                        <th>Comentario</th>
                        <th>Id usuario</th>
                    </tr>
                </thead>
                <tbody>
                {listComentarios.map((Comentario) => (
                    <ReviewLstItem key={Comentario.id_resena} {...Comentario} />
                ))}
                </tbody>
            </table>
        </div>
    </div>
    );
};
