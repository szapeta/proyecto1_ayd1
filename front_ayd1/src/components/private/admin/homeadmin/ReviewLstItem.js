import React from "react";

export const ReviewLstItem = ({
    id_resena,
    comentario,
    id_usuario
}) => {
    return (
        <tr>
            <td>{id_resena}</td>
            <td>{comentario}</td>
            <td>{id_usuario}</td>
        </tr>
    );
};
