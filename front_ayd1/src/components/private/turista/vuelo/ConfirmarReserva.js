import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";

export const ConfirmarVuelo = () => {
    const [idVuelo, setIdVuelo] = useState("");
    const [Origen, setOrigen] = useState("");
    const [Destino, setDestino] = useState("");
    const [Precio, setPrecio] = useState("");
    const [cant_asientos, setCantAsiento] = useState(0);

    const search = useLocation().search;
    const idvuelo = new URLSearchParams(search).get("idvuelo");
    console.log(idvuelo)

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);

    const handleConfirmar = (e) => {
        e.preventDefault();
        
        axios
            .post(types.apiurl + `addReservaVuelo`, { 
                id_vuelo: idvuelo,
                cantida_asiento: cant_asientos,
                id_user: user
            })
            .then((res) => {
                console.log("respuesta:", res);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        axios
            .post(types.apiurl + `listvuelosusu`, {
                id_vuelo:  idvuelo
            })
            .then((res) => {
                setIdVuelo(idvuelo)
                setOrigen(res.data[0].origen)
                setDestino(res.data[0].destino)
                setPrecio(res.data[0].precio)
                console.log(res.data)
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div className="container mt-5">
            <h3>Confirmar reservación</h3>
            <hr />
            <div className="form-group">
                <label>Id vuelo</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="id"
                    value={idVuelo}
                    onChange={(event) => {
                        setIdVuelo(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Origen</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="origen"
                    value={Origen}
                    onChange={(event) => {
                        setOrigen(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Destino</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="destino"
                    value={Destino}
                    onChange={(event) => {
                        setDestino(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Precio</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="precio"
                    value={Precio}
                    onChange={(event) => {
                        setPrecio(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Cantidad asientos</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="cant_asientos"
                    value={cant_asientos}
                    onChange={(event) => {
                        setCantAsiento(event.target.value);
                    }}
                />
            </div>

            <button className="btn btn-outline-primary" onClick={handleConfirmar}>
                Confirmar
            </button>
        </div>
    );
};
