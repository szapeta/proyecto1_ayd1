import React from "react";
import { useHistory } from "react-router-dom";

export const ReservasLstItem = ({
    id_vuelo,
    origen,
    destino,
    precio,
    fecha
}) => {;

    let history = useHistory();
    const handleReservar = (e) => {
        history.push(`/confirmarvuelo?idvuelo=${id_vuelo}`);
    };

    return (
        <tr>
            <td>{id_vuelo}</td>
            <td>{origen}</td>
            <td>{destino}</td>
            <td>{precio}</td>
            <td>{fecha}</td>
            <td>
                <button
                    className="btn btn-outline-primary"
                    onClick={handleReservar}
                >
                    Reservar
                </button>
            </td>
        </tr>
    );
};
