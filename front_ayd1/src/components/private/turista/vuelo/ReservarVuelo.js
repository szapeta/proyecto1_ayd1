import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";
import { ReservasLstItem } from "./VuelosLstItem";

export const ReservarVuelo = () => {
    const [listVuelos, setListVuelos] = useState([])
    const [origen, setOrigen] = useState("");
    const [destino, setDestino] = useState("");
    const [precio, setPrecio] = useState(0);
    const [fecha, setFecha] = useState("");

    useEffect(() => {
        axios
            .post(types.apiurl + `listareservasvuelosusuarios`,{
                origen: origen,
                destino: destino,
                precio: precio,
                fecha: fecha
            })
            .then((res) => {
                setListVuelos(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const handleSearch = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `listareservasvuelosusuarios`, {
                origen: origen,
                destino: destino,
                precio: precio,
                fecha: fecha
            })
            .then((res) => {
                console.log("respuesta:", res);
                setListVuelos(res.data);
            })
            .catch((err) => {
                console.log(err);
            });


    };

    return (
        <div>
            <div className=" pb-5 mb-5">
                <h2>Listado de vuelos disponibles</h2>
                <hr />

                <div className="row">
                    <div className="col-5">
                    </div>
                    <div className="col-5">
                        <form onSubmit={handleSearch}>
                            <label>Origen:</label>
                            <input
                                type="text"
                                placeholder="origen"
                                className="form-control"
                                name="origen"
                                autoComplete="off"
                                onChange={(event) => {
                                    setOrigen(event.target.value);
                                }}
                            />

                            <label>Destino:</label>
                            <input
                                type="text"
                                placeholder="destino"
                                className="form-control"
                                name="destino"
                                autoComplete="off"
                                onChange={(event) => {
                                    setDestino(event.target.value);
                                }}
                            />
                            <label>Precio:</label>
                            <input
                                type="text"
                                placeholder="precio"
                                className="form-control"
                                name="precioMax"
                                autoComplete="off"
                                value={precio}
                                onChange={(event) => {
                                    setPrecio(event.target.value);
                                }}
                            />
                            <label>Fecha:</label>
                            <input
                                type="date"
                                placeholder="fecha"
                                className="form-control"
                                name="fecha"
                                autoComplete="off"
                                onChange={(event) => {
                                    setFecha(event.target.value);
                                }}
                            />
                            <button
                                type="submit"
                                className="btn m-1 btn-block btn-outline-primary"
                            >
                                Buscar
                            </button>
                        </form>
                    </div>
                </div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>id Vuelo</th>
                            <th>Origen</th>
                            <th>Destino</th>
                            <th>Precio</th>
                            <th>Fecha </th>
                        </tr>
                    </thead>
                    <tbody>
                        {listVuelos.map((vuelo) => (
                            <ReservasLstItem key={vuelo.idvuelo} {...vuelo} />
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};
