import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";

export const ConfirmarHotel = () => {
    const [NoHabitacion, setNoHabitacion] = useState("");
    const [Ciudad, setCiudad] = useState("");
    const [Capacidad, setCapacidad] = useState("");
    const [Precio, setPrecio] = useState("");
    const [fechaInicio, setFechaInico] = useState("");
    const [fechaFin, setFechaFin] = useState("");

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);
    const search = useLocation().search;
    const idhabitacion = new URLSearchParams(search).get("idhabitacion");
    console.log(idhabitacion)

    const handleConfirmar = (e) => {
        e.preventDefault();

        axios
            .post(types.apiurl + `addreservahotel`, {
                fecha_inicio: fechaInicio,
                fecha_fin: fechaFin,
                id_user: user,
                id_habitacion: idhabitacion
            })
            .then((res) => {
                console.log(res)
            })
            .catch((err) => {
                console.log(err);
            });
    };


    useEffect(() => {
        axios
            .post(types.apiurl + `listreserva`, {
                id_habitacion: idhabitacion,
                id_user: user
            })
            .then((res) => {
                setNoHabitacion(idhabitacion)
                setCiudad(res.data.ciudad)
                setCapacidad(res.data.capacidad)
                setPrecio(res.data.precio)
                console.log(res.data)
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div className="container mt-5">
            <h2>Confirmar reservación</h2>
            <hr />
            <div className="form-group">
                <label>No. de habitación</label>
                <input
                    type="ingrese el numero de habitacion"
                    autoComplete="off"
                    className="form-control"
                    id="id"
                    value={NoHabitacion}
                    onChange={(event) => {
                        setNoHabitacion(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Ciudad</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="ciudad"
                    value={Ciudad}
                    onChange={(event) => {
                        setCiudad(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Capacidad</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="capacidad"
                    value={Capacidad}
                    onChange={(event) => {
                        setCapacidad(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Precio</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="precio"
                    value={Precio}
                    onChange={(event) => {
                        setPrecio(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Fecha inicio</label>
                <input
                    type="date"
                    autoComplete="off"
                    className="form-control"
                    id="fechaInicio"
                    onChange={(event) => {
                        setFechaInico(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Fecha fin</label>
                <input
                    type="date"
                    autoComplete="off"
                    className="form-control"
                    id="fechaFin"
                    onChange={(event) => {
                        setFechaFin(event.target.value);
                    }}
                />
            </div>
            <button className="btn btn-outline-primary" onClick={handleConfirmar}>
                Confirmar
            </button>
        </div>
    );
};
