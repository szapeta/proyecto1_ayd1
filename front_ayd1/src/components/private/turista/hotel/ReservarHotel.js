import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";
import {ReservasLstItem} from "./HabitacionesLstItem";

export const ReservarHotel = () => {
    const [listHabitaciones, setListHabitaciones] = useState([])
    const [ciudad, setCiudad] = useState("");
    const [cantidadPersonas, setCantidadPersonas] = useState(0);
    const [precio, setPrecio] = useState(0);
    const [fecha, setFecha] = useState("");

    useEffect(() => {
        axios
            .post(types.apiurl + `busquedahabitaciones`,{
                fecha: fecha,
                ciudad: ciudad,
                precio: precio,
                cant_personas: cantidadPersonas
            })
            .then((res) => {
                setListHabitaciones(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const handleSearch = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `busquedahabitaciones`, {
                fecha: fecha,
                ciudad: ciudad,
                precio: precio,
                cant_personas: cantidadPersonas

            })
            .then((res) => {
                console.log("respuesta:", res);
                setListHabitaciones(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
            

    };

    return (
        <div>
            <div className=" pb-5 mb-5">
                <h2>Listado de habitaciones disponibles</h2>
                <hr />

                <div className="row">
                    <div className="col-5">
                    </div>
                    <div className="col-5">
                        <form onSubmit={handleSearch}>

                            <label>Ciudad:</label>
                            <input
                                type="text"
                                placeholder="ciudad"
                                className="form-control"
                                name="ciudad"
                                autoComplete="off"
                                value={ciudad}
                                onChange={(event) => {
                                    setCiudad(event.target.value);
                                }}
                            />
                            <label>Cantidad de personas:</label>
                            <input
                                type="text"
                                placeholder="ciudad"
                                className="form-control"
                                name="cantidadPersonas"
                                autoComplete="off"
                                value={cantidadPersonas}
                                onChange={(event) => {
                                    setCantidadPersonas(event.target.value);
                                }}
                            />
                            <label>Precio:</label>
                            <input
                                type="text"
                                placeholder="precio"
                                className="form-control"
                                name="precioMin"
                                autoComplete="off"
                                onChange={(event) => {
                                    setPrecio(event.target.value);
                                }}
                            />
                            <label>Fecha:</label>
                            <input
                                type="date"
                                placeholder="fechaInicio"
                                className="form-control"
                                name="fecha"
                                autoComplete="off"
                                onChange={(event) => {
                                    setFecha(event.target.value);
                                }}
                            />
                            <button
                                type="submit"
                                className="btn m-1 btn-block btn-outline-primary"
                            >
                                Buscar
                            </button>
                        </form>
                    </div>
                </div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Id habitación</th>
                            <th>Tipo</th>
                            <th>Precio</th>
                            <th>Fecha</th>
                            <th>Cantidad de personas</th>
                            <th>Ciudad</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listHabitaciones.map((habitacion) => (
                            <ReservasLstItem key={habitacion.idhabitacion} {...habitacion} />
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};
