import axios from "axios";
import { types } from "../../../../types/Types";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { useHistory } from "react-router-dom";


export const ReservasLstItem = ({
    id_habitacion,
    tipo,
    precio,
    fecha,
    capacidad,
    ciudad
}) => {
    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);

    let history = useHistory();

    const handleReservar = (e) => {
        history.push(`/confirmarhotel?idhabitacion=${id_habitacion}`);
    };

    return (
        <tr>
            <td>{id_habitacion}</td>
            <td>{tipo}</td>
            <td>{precio}</td>
            <td>{fecha}</td>
            <td>{capacidad}</td>
            <td>{ciudad}</td>
            <td>
                <button
                    className="btn btn-outline-primary"
                    onClick={handleReservar}
                >
                    Reservar
                </button>
            </td>
        </tr>
    );
};
