import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";
import {VehiculoLstItem} from "./VehiculoLstItem";

export const ReservarVehiculo = () => {
    const [listVehiculos, setListVehiculos] = useState([])
    const [marca, setMarca] = useState("");
    const [placa, setPlaca] = useState("");
    const [modelo, setModelo] = useState("");
    const [precio, setPrecio] = useState(0);

    useEffect(() => {
        axios
            .post(types.apiurl + `listvehiculosusuario`,{
                marca: marca,
                placa: placa,
                modelo: modelo,
                precio: precio
            })
            .then((res) => {
                setListVehiculos(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const handleSearch = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `listvehiculosusuario`, {
                marca: marca,
                placa: placa,
                modelo: modelo,
                precio: precio
            })
            .then((res) => {
                console.log("respuesta:", res);
                setListVehiculos(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
            

    };

    return (
        <div>
            <div className=" pb-5 mb-5">
                <h2>Listado de vehiculos disponibles</h2>
                <hr />

                <div className="row">
                    <div className="col-5">
                    </div>
                    <div className="col-5">
                        <form onSubmit={handleSearch}>
                            <label>Marca:</label>
                            <input
                                type="text"
                                placeholder="marca"
                                className="form-control"
                                name="marca"
                                autoComplete="off"
                                onChange={(event) => {
                                    setMarca(event.target.value);
                                }}
                            />

                            <label>Placa:</label>
                            <input
                                type="text"
                                placeholder="placa"
                                className="form-control"
                                name="placa"
                                autoComplete="off"
                                onChange={(event) => {
                                    setPlaca(event.target.value);
                                }}
                            />
                            <label>Modelo:</label>
                            <input
                                type="text"
                                placeholder="modelo"
                                className="form-control"
                                name="modelo"
                                autoComplete="off"
                                onChange={(event) => {
                                    setModelo(event.target.value);
                                }}
                            />
                            <label>Precio:</label>
                            <input
                                type="text"
                                placeholder="precio"
                                className="form-control"
                                name="precio"
                                autoComplete="off"
                                value={precio}
                                onChange={(event) => {
                                    setPrecio(event.target.value);
                                }}
                            />
                            <button
                                type="submit"
                                className="btn m-1 btn-block btn-outline-primary"
                            >
                                Buscar
                            </button>
                        </form>
                    </div>
                </div>

                <table className="table">
                    <thead>
                        <tr>
                        
                            <th>Marca</th>
                            <th>Placa</th>
                            <th>Modelo</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listVehiculos.map((vehiculo) => (
                            <VehiculoLstItem key={vehiculo.idvehiculo} {...vehiculo} />
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};
