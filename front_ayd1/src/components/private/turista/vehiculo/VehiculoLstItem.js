import React from "react";
import { useHistory } from "react-router-dom";

export const VehiculoLstItem = ({
    marca,
    placa,
    modelo,
    precio,
}) => {

    let history = useHistory();
    const handleReservar = (e) => {
        history.push(`/confirmarauto?idvehiculo=${placa}`);
    };

    return (
        <tr>
            
            <td>{marca}</td>
            <td>{placa}</td>
            <td>{modelo}</td>
            <td>{precio}</td>
            <td>
                <button
                    className="btn btn-outline-primary"
                    onClick={handleReservar}
                >
                    Reservar
                </button>
            </td>
        </tr>
    );
};
