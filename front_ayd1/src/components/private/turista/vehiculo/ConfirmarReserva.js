import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";

export const ConfirmarAuto = () => {
    const [marca, setMarca] = useState("");
    const [placa, setPlaca] = useState("");
    const [modelo, setModelo] = useState("");
    const [precio, setPrecio] = useState(0);
    const [fechaInicio, setFechaInico] = useState("");
    const [fechaFin, setFechaFin] = useState("");

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);

    const search = useLocation().search;
    const idplaca = new URLSearchParams(search).get("idvehiculo");
    console.log(idplaca)
    
    const handleConfirmar = (e) => {
        e.preventDefault();
        
        axios
            .post(types.apiurl + `addReservaAuto`, { 
                placa: idplaca,
                fecha_inicio: fechaInicio,
                fecha_final: fechaFin,
                id_user: user
            })
            .then((res) => {
                console.log("respuesta:", res);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        axios
            .post(types.apiurl + `getAuto`, {
                placa:  placa
            })
            .then((res) => {
                setPlaca(idplaca)
                setMarca(res.data.marca)
                setModelo(res.data.modelo)
                setPrecio(res.data.precio)
                console.log(res.data)
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div className="container mt-5">
            <h2>Confirmar reservación</h2>
            <hr />
            <div className="form-group">
                <label>Marca</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="marca"
                    value={marca}
                    onChange={(event) => {
                        setMarca(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Placa</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="placa"
                    value={placa}
                    onChange={(event) => {
                        setPlaca(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Modelo</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="modelo"
                    value={modelo}
                    onChange={(event) => {
                        setModelo(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Precio</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="precio"
                    value={precio}
                    onChange={(event) => {
                        setPrecio(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Fecha inicio</label>
                <input
                    type="date"
                    autoComplete="off"
                    className="form-control"
                    id="fechaInicio"
                    onChange={(event) => {
                        setFechaInico(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Fecha fin</label>
                <input
                    type="date"
                    autoComplete="off"
                    className="form-control"
                    id="fechaFin"
                    onChange={(event) => {
                        setFechaFin(event.target.value);
                    }}
                />
            </div>
            <button className="btn btn-outline-primary" onClick={handleConfirmar}>
                Confirmar
            </button>
        </div>
    );
};
