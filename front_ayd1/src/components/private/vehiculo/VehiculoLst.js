import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { AuthContext } from '../../../auth/AuthContext';
import { types } from '../../../types/Types';
import { VehiculoLstItem } from './VehiculoLstItem';

export const VehiculoLst = () => {

    const {
        userLogged: { user, name, path, rol},
        dispatch,
    } = useContext(AuthContext);

    const [listVehiculos, setListVehiculos] = useState([])
    const [marca, setMarca] = useState("");
    const [placa, setPlaca] = useState("");
    const [modelo, setModelo] = useState("");
    
    const handleSearch = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `listvehiculos`,{
                id_usuario:user, marca:marca, placa:placa, modelo:modelo
            })
            .then((res) => {
                setListVehiculos(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        axios
            .post(types.apiurl + `listvehiculos`,{
                id_usuario:user, marca:"", placa:"", modelo:"", precio:""
            }
            )
            .then((res) => {
                setListVehiculos(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

  return (
    <div>
            <div className=" pb-5 mb-5">
                <h1>Listado de Vehiculos</h1>
                <hr />

                <div className="row">
                    <div className="col-5">
                        <br />
                        <Link
                            className="btn btn-success"
                            to={`./vehiculosadmnew?user=new`}
                        >
                            Ingreso de Vehiculos
                        </Link>
                    </div>
                    <div className="col-5">
                        <form onSubmit={handleSearch}>
                            <label>Marca:</label>
                            <input
                                type="text"
                                placeholder="marca"
                                className="form-control"
                                name="marca"
                                autoComplete="off"
                                value={marca}
                                onChange={(event) => {
                                    setMarca(event.target.value);
                                }}
                            />

                            <label>Placa:</label>
                            <input
                                type="text"
                                placeholder="placa"
                                className="form-control"
                                name="placa"
                                autoComplete="off"
                                value={placa}
                                onChange={(event) => {
                                    setPlaca(event.target.value);
                                }}
                            />

                            <div className="form-group">
                                <label>Modelo:</label>
                                <input
                                type="text"
                                placeholder="modelo"
                                className="form-control"
                                name="modelo"
                                autoComplete="off"
                                value={modelo}
                                onChange={(event) => {
                                    setModelo(event.target.value);
                                }}
                            />
                            </div>
                            <button
                                type="submit"
                                className="btn m-1 btn-block btn-outline-primary"
                            >
                                Buscar
                            </button>
                        </form>
                    </div>
                </div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Marca</th>
                            <th>Placa</th>
                            <th>Modelo</th>
                            <th>Precio</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    {listVehiculos.map((item) => (
                        <VehiculoLstItem key={item.id_servicio} {...item} />
                    ))}
                    </tbody>
                </table>
            </div>
        </div>
  )
}
