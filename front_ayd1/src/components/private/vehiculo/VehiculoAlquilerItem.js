import React from "react";

export const VehiculoAlquilerItem = ({
    idvehiculo,
    marca,
    placa,
    modelo,
    precio,
    ciudad
}) => {
    const handleEditar = () => {};
    const handleEliminar = () => {};

    return (
        <tr>
            <td>{placa}</td>
            <td>{marca}</td>
            <td>{modelo}</td>
            <td>{precio}</td>
            <td>
                <button
                    className="btn btn-outline-primary"
                    onClick={handleEditar}
                >
                    Editar
                </button>
                {"  "}
                <button
                    className="btn btn-outline-danger"
                    onClick={handleEliminar}
                >
                    Eliminar
                </button>
            </td>
        </tr>
    );
};
