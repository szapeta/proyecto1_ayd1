import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../../../auth/AuthContext";
import { types } from "../../../types/Types";

export const VehiculoNew = () => {

    const {
        userLogged: { user, name, path, },
        dispatch,
    } = useContext(AuthContext);
    const [marca, setMarca] = useState("");
    const [placa, setPlaca] = useState("");
    const [modelo, setModelo] = useState("");
    const [precio, setPrecio] = useState("");
    const [ciudad, setCiudad] = useState("");
    const [lstCiudad, setLstCiudad] = useState([]);
    const [lstMarcas, setLstMarcas] = useState([]);

    const handleGuardar = async (e) => {
        e.preventDefault();

        axios.post(types.apiurl + "addAuto",{
            servicio:user,
            marca,
            placa,
            modelo,
            precio,
            ciudad
        }).then(resp=>{
            console.log(resp)
            if(resp.data.msg==""){
                let ve = resp.data[0];
                console.log("respuesta:",ve.idvehiculo);
                alert(`Vehiculo ${ve.idvehiculo} agregado con exito.`)
                setMarca("");
                setPlaca("");
                setModelo("");
                setPrecio("");
            }else{
                alert(resp.data.msg);
            }
        })
    };
    
    useEffect(() => {
        axios.get(types.apiurl+"listciudad")
        .then(resp=>{
            setLstCiudad(resp.data);
        })

        axios.get(types.apiurl+"listmarcas")
        .then(resp=>{
            setLstMarcas(resp.data);
        })

    }, [])
    

    return (
        <div>
            <div className="pb-5 mb-5">
                <Link className="btn btn-success" to={`./vehiculos`}>
                    regresar
                </Link>
                <hr />
                <h1>Vehiculos</h1>
                <form>
                    <div class="form-group">
                        <label>Marca:</label>
                        <select class="form-control" onChange={(event) => {
                                setMarca(event.target.value);
                            }}>
                                <option value="-1" selected>
                                -Elija una marca:-
                            </option>
                            {lstMarcas.map((option) => (
                            <option value={option.id_marca}>{option.marca}</option>
                            ))}
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Placa:</label>
                        <input
                            type="text"
                            class="form-control"
                            id="placa"
                            placeholder=""
                            value={placa}
                            onChange={(event) => {
                                setPlaca(event.target.value);
                            }}
                        />
                    </div>
                    <div class="form-group">
                        <label>Modelo:</label>
                        <input
                            type="text"
                            class="form-control"
                            id="modelo"
                            placeholder=""
                            value={modelo}
                            onChange={(event) => {
                                setModelo(event.target.value);
                            }}
                        />
                    </div>
                    <div class="form-group">
                        <label>Precio:</label>
                        <input
                            type="text"
                            class="form-control"
                            id="precio"
                            placeholder=""
                            value={precio}
                            onChange={(event) => {
                                setPrecio(event.target.value);
                            }}
                        />
                    </div>
                    <div class="form-group">
                        <label>Ciudad:</label>
                        <select class="form-control" onChange={(event) => {
                                setCiudad(event.target.value);
                            }}>
                                <option value="-1" selected>
                                -Elija una ciudad:-
                            </option>
                            {lstCiudad.map((option) => (
                            <option value={option.id_ciudad}>{option.ciudad}</option>
                            ))}
                        </select>
                    </div>
                    <button
                        type="submit"
                        class="btn btn-primary"
                        onClick={handleGuardar}
                    >
                        Guardar
                    </button>
                </form>
            </div>
        </div>
    );
};
