import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { AuthContext } from '../../../auth/AuthContext';
import { types } from '../../../types/Types';
import { VehiculoAlquilerItem } from './VehiculoAlquilerItem';
import { VehiculoLstItem } from './VehiculoLstItem';

export const VehiculoLstAlquiler = () => {

    const {
        userLogged: { user, name, path, rol },
        dispatch,
    } = useContext(AuthContext);


    const [listVehiculos, setListVehiculos] = useState([])
    const [nombremarca, setNombre] = useState("")
    const [pais, setPais] = useState("");
    const [ciudad, setCiudad] = useState("");
    
    const handleSearch = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `getAlquilerAutoId`, {
               id_usuario:user
            })
            .then((res) => {
                console.log("respuesta:", res);
                setListVehiculos(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        console.log(user);
      axios.post(types.apiurl+"getAlquilerAutoId",{
        id_usuario:user
      }).then(resp=>{


        let val = resp.data[0]
        setListVehiculos(val)
      })
    }, [])
    

  return (
    <div>
            <div className=" pb-5 mb-5">
                <h1>Listado de Vehiculos Alquilados</h1>
                <hr />

                <div className="row">
                    <div className="col-5">
                    </div>
                    <div className="col-5">
                        <form onSubmit={handleSearch}>
                            <label>Marca:</label>
                            <input
                                type="text"
                                placeholder="marca"
                                className="form-control"
                                name="marca"
                                autoComplete="off"
                                value={nombremarca}
                                onChange={(event) => {
                                    setNombre(event.target.value);
                                }}
                            />
                            <label>Pais:</label>
                            <input
                                type="text"
                                placeholder="pais"
                                className="form-control"
                                name="pais"
                                autoComplete="off"
                                value={pais}
                                onChange={(event) => {
                                    setPais(event.target.value);
                                }}
                            />

                            <div className="form-group">
                                <label>Ciudad:</label>
                                <input
                                type="text"
                                placeholder="ciudad"
                                className="form-control"
                                name="ciudad"
                                autoComplete="off"
                                value={ciudad}
                                onChange={(event) => {
                                    setCiudad(event.target.value);
                                }}
                            />
                            </div>
                            <button
                                type="submit"
                                className="btn m-1 btn-block btn-outline-primary"
                            >
                                Buscar
                            </button>
                        </form>
                    </div>
                </div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Placa</th>
                            <th>Modelo</th>
                            <th>Precio</th>
                            <th>Marca</th>
                            <th>Ciudad</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    {listVehiculos.map((Vehiculo) => (
                        <VehiculoAlquilerItem key={Vehiculo.placa} {...Vehiculo} />
                    ))}
                    </tbody>
                </table>
            </div>
        </div>
  )
}
