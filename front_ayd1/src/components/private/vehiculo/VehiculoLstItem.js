import React from "react";

export const VehiculoLstItem = ({
    id_servicio,
    marca,
    placa,
    modelo,
    precio
}) => {
    const handleEditar = () => {};
    const handleEliminar = () => {};

    return (
        <tr>
            <td>{marca}</td>
            <td>{placa}</td>
            <td>{modelo}</td>
            <td>{precio}</td>
            <td>
                <button
                    className="btn btn-outline-primary"
                    onClick={handleEditar}
                >
                    Editar
                </button>
                {"  "}
                <button
                    className="btn btn-outline-danger"
                    onClick={handleEliminar}
                >
                    Eliminar
                </button>
            </td>
        </tr>
    );
};
