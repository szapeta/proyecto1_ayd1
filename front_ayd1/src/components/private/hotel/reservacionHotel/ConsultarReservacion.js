import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";
import { ReservasLstItem } from './ReservasLstItem';

export const ConsultarReservacion = () => {
    const [listReservas, setListReservas] = useState([])
    const [fechaInicio, setFechaInicio] = useState("");  
    const [fechaFin, setFechaFin] = useState("");  
    const [precio, setPrecio] = useState(0);  

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);
    
    const handleSearch = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `busquedarservas`,{
                id_servicio: user,
                fecha:fechaInicio,
                fecha_fin:fechaFin,
            })
            .then((res) => {
                console.log("respuesta:", res);
                setListReservas(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };
    useEffect(() => {
        axios
            .post(types.apiurl + `busquedarservas`,{
                id_servicio: user,
                fecha:fechaInicio,
                fecha_fin:fechaFin,
            })
            .then((res) => {
                setListReservas(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div>
        <div className=" pb-5 mb-5">
            <h1>Listado de reservas</h1>
            <hr />

            <div className="row">
                <div className="col-5">
                    <form onSubmit={handleSearch}>
                        <label>Fecha inicio:</label>
                        <input
                            type="date"
                            placeholder="fecha inicio"
                            className="form-control"
                            name="fechainicio"
                            autoComplete="off"
                            value={fechaInicio}
                            onChange={(event) => {
                                setFechaInicio(event.target.value);
                            }}
                        />

                        <label>Fecha fin:</label>
                        <input
                            type="date"
                            placeholder="fecha fin"
                            className="form-control"
                            name="fechafin"
                            autoComplete="off"
                            value={fechaFin}
                            onChange={(event) => {
                                setFechaFin(event.target.value);
                            }}
                        />

                        <button
                            type="submit"
                            className="btn m-1 btn-block btn-outline-primary"
                        >
                            Buscar
                        </button>
                    </form>
                </div>
            </div>

            <table className="table">
                <thead>
                    <tr>
                        <th>No. habitación</th>
                        <th>Fecha inicio</th>
                        <th>Fecha fin</th>
                    </tr>
                </thead>
                <tbody>
                {listReservas.map((Reserva) => (
                    <ReservasLstItem key={Reserva.idhabitacion} {...Reserva} />
                ))}
                </tbody>
            </table>
        </div>
    </div>
    );
};
