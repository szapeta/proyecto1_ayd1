import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";

export const EliminarHabitacion = () => {
    const [NoHabitacion, setNoHabitacion] = useState("");   

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);

    const handleEliminar = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `drophabitacion`, {
                id_habitacion: NoHabitacion
            })
            .then((res) => {
                console.log("respuesta:", res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="container mt-5">
            <h1>Eliminar habitación</h1>
            <hr />
            <div className="form-group">
                <label>No. de habitación</label>
                <input
                    type="ingrese el numero de habitacion"
                    autoComplete="off"
                    className="form-control"
                    id="id"
                    onChange={(event) => {
                        setNoHabitacion(event.target.value);
                    }}
                />
            </div>
            <button className="btn btn-outline-primary" onClick={handleEliminar}>
                Eliminar habitación
            </button>
        </div>
    );
};
