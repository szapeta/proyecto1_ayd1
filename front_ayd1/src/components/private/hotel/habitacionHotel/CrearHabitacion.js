import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";

export const CrearHabitacion = () => {
    const [NoHabitacion, setNoHabitacion] = useState("");
    const [Tipo, setTipo] = useState("");
    const [Precio, setPrecio] = useState("");
    const [Capacidad, setCapacidad] = useState("");
    const [Descripcion, setDescripcion] = useState("");
    const [fecha, setFecha] = useState("");
    const [ciudad, setCiudad] = useState("");
    const [lstCiudad, setLstCiudad] = useState([]);



    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);

    const handleCrear = (e) => {
        e.preventDefault();

        axios
            .post(types.apiurl + `addhabitacion`, {
                No_habitacion: NoHabitacion,
                Tipo: Tipo,
                Capacidad: Capacidad,
                Descip: Descripcion,
                Fecha: fecha,
                Id_Servicio: user,
                Precio: Precio,
                Ciudad: ciudad
            })
            .then((res) => {
                console.log("respuesta:", res);
            })
            .catch((err) => {
                console.log(err);
            });
    };
    useEffect(() => {
        axios.get(types.apiurl+"listciudad")
        .then(resp=>{
            console.log(resp.data)

            setLstCiudad(resp.data);
        })

    }, [])

    return (
        <div className="container mt-5">
            <h2>Crear habitación</h2>
            <hr />
            <div className="form-group">
                <label>No. de habitación</label>
                <input
                    type="ingrese el numero de habitacion"
                    autoComplete="off"
                    className="form-control"
                    id="id"
                    onChange={(event) => {
                        setNoHabitacion(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Tipo</label>
                <input
                    type="ingrese el tipo de habitacion"
                    autoComplete="off"
                    className="form-control"
                    id="tipo"
                    onChange={(event) => {
                        setTipo(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Precio</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="precio"
                    onChange={(event) => {
                        setPrecio(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Capacidad</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="capacidad"
                    onChange={(event) => {
                        setCapacidad(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Descripción</label>
                <input
                    type="text"
                    autoComplete="off"
                    className="form-control"
                    id="descripcion"
                    onChange={(event) => {
                        setDescripcion(event.target.value);
                    }}
                />
            </div>
            <div class="form-group">
                <label>Ciudad:</label>
                <select class="form-control" onChange={(event) => {
                    setCiudad(event.target.selectedIndex);
                }}>
                    <option value="-1" selected>
                        -Elija una ciudad:-
                    </option>
                    {lstCiudad.map((option) => (
                        <option value={option.id_ciudad}>{option.ciudad}</option>
                    ))}
                </select>
            </div>
            <div className="form-group">
                <label>Fecha</label>
                <input
                    type="date"
                    autoComplete="off"
                    className="form-control"
                    id="fechaInicio"
                    onChange={(event) => {
                        setFecha(event.target.value);
                    }}
                />
            </div>
            <button className="btn btn-outline-primary" onClick={handleCrear}>
                Crear habitación
            </button>
        </div>
    );
};
