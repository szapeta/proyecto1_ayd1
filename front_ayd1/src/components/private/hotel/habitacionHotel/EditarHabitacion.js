import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../auth/AuthContext";
import { types } from "../../../../types/Types";

export const EditarHabitacion = () => {
    const [NoHabitacion, setNoHabitacion] = useState("");
    const [Tipo, setTipo] = useState("");
    const [Precio, setPrecio] = useState("");
    const [Capacidad, setCapacidad] = useState("");
    const [Descripcion, setDescripcion] = useState("");   
    const [fechaInicio, setFechaInico] = useState("");
    const [fechaFin, setFechaFin] = useState("");  

    const {
        userLogged: { user, name, path },
        dispatch,
    } = useContext(AuthContext);

    const handleEditar = (e) => {
        e.preventDefault();
        axios
            .post(types.apiurl + `editarhabitacion`, { 
                nohabitacion: NoHabitacion, 
                tipo: Tipo, precio: Precio, 
                capacidad: Capacidad, 
                descripcion: Descripcion,
                fechainicio: fechaInicio,
                fechafin: fechaFin
            })
            .then((res) => {
                console.log("respuesta:", res);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="container mt-5">
            <h2>Editar habitación</h2>
            <hr />
            <div className="form-group">
                <label>No. de habitación</label>
                <input
                    type="ingrese el numero de habitacion"
                    autoComplete="off"
                    className="form-control"
                    id="id"
                    onChange={(event) => {
                        setNoHabitacion(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Tipo</label>
                <input
                    type="ingrese el tipo de habitacion"
                    autoComplete="off"
                    className="form-control"
                    id="tipo"
                    onChange={(event) => {
                        setTipo(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Precio</label>
                <input
                    type="ingrese el precio de la habitacion"
                    autoComplete="off"
                    className="form-control"
                    id="precio"
                    onChange={(event) => {
                        setPrecio(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Capacidad</label>
                <input
                    type="ingrese la capacidad de la habitacion"
                    autoComplete="off"
                    className="form-control"
                    id="capacidad"
                    onChange={(event) => {
                        setCapacidad(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Descripción</label>
                <input
                    type="ingrese la descripcion de la habitacion"
                    autoComplete="off"
                    className="form-control"
                    id="descripcion"
                    onChange={(event) => {
                        setDescripcion(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Fecha inicio</label>
                <input
                    type="date"
                    autoComplete="off"
                    className="form-control"
                    id="fechaInicio"
                    onChange={(event) => {
                        setFechaInico(event.target.value);
                    }}
                />
            </div>
            <div className="form-group">
                <label>Fecha Fin</label>
                <input
                    type="date"
                    autoComplete="off"
                    className="form-control"
                    id="fechaFin"
                    onChange={(event) => {
                        setFechaFin(event.target.value);
                    }}
                />
            </div>
            <button className="btn btn-outline-primary" onClick={handleEditar}>
                Actualizar habitación
            </button>
        </div>
    );
};
