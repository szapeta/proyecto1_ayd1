require('chromedriver');
const { Builder, By, Key, until } = require('selenium-webdriver');
var webdriver = require('selenium-webdriver');
var assert = require("chai").assert;

describe('Crear_Turista_Correcto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Crear_Turista_Correcto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//a[contains(@href, \'/new\')]")).click()
    {
      const element = await driver.findElement(By.xpath("//a[contains(@href, \'/new\')]"))
      // await driver.actions({ bridge: true }).moveToElement(element).perform()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).sendKeys("Turista")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).sendKeys("2022-07-13")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).sendKeys("turista@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[4]/select")).click()
    {
      const dropdown = await driver.findElement(By.id("idrol"))
      await dropdown.findElement(By.xpath("//option[. = 'Turista']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Registrarse\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Registrarse\')]")).click()
  })
})

describe('Crear_Turista_Incorrecto', function() {
  
  this.timeout(30000000000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Crear_Turista_Incorrecto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//a[contains(@href, \'/new\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).sendKeys("Turista")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).sendKeys("2022-07-13")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).sendKeys("turista@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[4]/select")).click()
    {
      const dropdown = await driver.findElement(By.id("idrol"))
      await dropdown.findElement(By.xpath("//option[. = 'Turista']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).click()
  })
})

describe('Crear_Servicio_Hotel', function() {
  this.timeout(30000000000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Crear_Servicio_Hotel', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//a[contains(@href, \'/new\')]")).click()
    {
      const element = await driver.findElement(By.xpath("//a[contains(@href, \'/new\')]"))
      // await driver.actions({ bridge: true }).moveToElement(element).perform()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).sendKeys("Hotel")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).sendKeys("2022-07-13")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).sendKeys("hotel@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[4]/select")).click()
    {
      const dropdown = await driver.findElement(By.id("idrol"))
      await dropdown.findElement(By.xpath("//option[. = 'Hotel']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Registrarse\')]")).click()
  })
})

describe('Crear_Servicio_Hotel_Incorrecto', function() {
  this.timeout(30000000000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Crear_Servicio_Hotel_Incorrecto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//a[contains(@href, \'/new\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).sendKeys("Hotel")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).sendKeys("2022-07-13")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).sendKeys("hotel@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[4]/select")).click()
    {
      const dropdown = await driver.findElement(By.id("idrol"))
      await dropdown.findElement(By.xpath("//option[. = 'Hotel']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//html")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).sendKeys("fff")
  })
})

describe('Crear_Servicio_Auto', function() {
  this.timeout(30000000000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Crear_Servicio_Auto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//a[contains(@href, \'/new\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).sendKeys("Auto")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).sendKeys("2022-07-13")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).sendKeys("auto@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[4]/select")).click()
    {
      const dropdown = await driver.findElement(By.id("idrol"))
      await dropdown.findElement(By.xpath("//option[. = 'Autos']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Registrarse\')]")).click()
  })
})

describe('Crear_Servicio_Avion', function() {
  this.timeout(30000000000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Crear_Servicio_Avion', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//a[contains(@href, \'/new\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div/input")).sendKeys("Avion")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[2]/input")).sendKeys("2022-07-13")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[3]/input")).sendKeys("avion@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[4]/select")).click()
    {
      const dropdown = await driver.findElement(By.id("idrol"))
      await dropdown.findElement(By.xpath("//option[. = 'Vuelos']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[5]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/form/div[6]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Registrarse\')]")).click()
  })
})

describe('Login_Correcto', function() {
  this.timeout(30000000000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Login_Correcto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("turista@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
  })
})

describe('Login_Incorrecto', function() {
  this.timeout(30000000000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Login_Incorrecto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("noexite@prueba.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
  })
})

describe('Libre', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Libre', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("hotel@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()

    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/crearhabitacion\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/crearhabitacion\')]")).click()
    {
      const element = await driver.findElement(By.xpath("//a[contains(@href, \'/crearhabitacion\')]"))
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("10")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("Simple")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).sendKeys("100")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).sendKeys("2")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("Habitacion simple para dos personas")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/select")).click()
    {
      const dropdown = await driver.findElement(By.css(".form-group:nth-child(8) > .form-control"))
      await dropdown.findElement(By.xpath("//option[. = 'Quezaltenango']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[7]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[7]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[7]/input")).sendKeys("2022-06-27")
    await driver.findElement(By.xpath("//button[contains(.,\'Crear habitación\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div")).click()
    await driver.findElement(By.xpath("//a[contains(@href, \'/crearhabitacion\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("20")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("Doble")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).sendKeys("200")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).sendKeys("4")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("Habitacion doble para cuatro personas")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/select")).click()
    {
      const dropdown = await driver.findElement(By.css(".form-group:nth-child(8) > .form-control"))
      await dropdown.findElement(By.xpath("//option[. = 'Retalhuleu']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[7]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[7]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[7]/input")).sendKeys("2022-06-26")
    await driver.findElement(By.xpath("//button[contains(.,\'Crear habitación\')]")).click()
  })
})

describe('Ingreso_Automovil_Correcto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Ingreso_Automovil_Correcto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("auto@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/vehiculos\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/vehiculos\')]")).click()
    await driver.findElement(By.xpath("//a[contains(@href, \'/vehiculosadmnew?user=new\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div/select")).click()
    {
      const dropdown = await driver.findElement(By.css(".form-group:nth-child(1) > .form-control"))
      await driver.wait(until.elementLocated(By.xpath("//option[. = 'Alfa Romeo']")), 50000)
      await dropdown.findElement(By.xpath("//option[. = 'Alfa Romeo']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[2]/input")).sendKeys("FFFF")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[3]/input")).sendKeys("2022")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[4]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[4]/input")).sendKeys("200")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[5]/select")).click()
    {
      const dropdown = await driver.findElement(By.css(".form-group:nth-child(5) > .form-control"))
      await dropdown.findElement(By.xpath("//option[. = 'Coban']")).click()
    }
    await driver.findElement(By.xpath("//button[contains(.,\'Guardar\')]")).click()
  })
})

describe('Ingreso_Automovil_Incorrecto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Ingreso_Automovil_Incorrecto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("auto@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/vehiculos\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/vehiculos\')]")).click()
    {
      const element = await driver.findElement(By.xpath("//a[contains(@href, \'/vehiculos\')]"))
      // await driver.actions({ bridge: true }).moveToElement(element).perform()
    }
    await driver.findElement(By.xpath("//a[contains(@href, \'/vehiculosadmnew?user=new\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div/select")).click()
    {
      const dropdown = await driver.findElement(By.css(".form-group:nth-child(1) > .form-control"))
      await driver.wait(until.elementLocated(By.xpath("//option[. = 'Alfa Romeo']")), 50000)
      await dropdown.findElement(By.xpath("//option[. = 'Alfa Romeo']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[2]/input")).sendKeys("FFFF")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[3]/input")).sendKeys("2022")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[5]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[4]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[4]/input")).sendKeys("200")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/form/div[5]/select")).click()
    {
      const dropdown = await driver.findElement(By.css(".form-group:nth-child(5) > .form-control"))
      await dropdown.findElement(By.xpath("//option[. = 'Coban']")).click()
    }
    await driver.findElement(By.xpath("//button[contains(.,\'Guardar\')]")).click()
  })
})

describe('Creacion_Vuelo_Correcto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Creacion_Vuelo_Correcto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Avion@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/crearvuelo\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/crearvuelo\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Guatemala")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).sendKeys("Mexico")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/select")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).sendKeys("500")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("40")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).sendKeys("2022-07-15")
    await driver.findElement(By.xpath("//button[contains(.,\'Crear habitación\')]")).click()
  })
})

describe('Creacion_Vuelo_Incorrecto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Creacion_Vuelo_Incorrecto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Avion@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/crearvuelo\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/crearvuelo\')]")).click()
    {
      const element = await driver.findElement(By.xpath("//a[contains(@href, \'/crearvuelo\')]"))
      // await driver.actions({ bridge: true }).moveToElement(element).perform()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Africa")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/select")).click()
    {
      const dropdown = await driver.findElement(By.css(".form-group:nth-child(4) > .form-control"))
      await dropdown.findElement(By.xpath("//option[. = 'Ida']")).click()
    }
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).sendKeys("Guatemala")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).sendKeys("500")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("40")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).sendKeys("2022-07-21")
  })
})

describe('Compra_Boleto_Correcto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Compra_Boleto_Correcto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Turista@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/reservarvuelo\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/reservarvuelo\')]")).click()
    {
      const element = await driver.findElement(By.xpath("//a[contains(@href, \'/reservarvuelo\')]"))
      // await driver.actions({ bridge: true }).moveToElement(element).perform()
    }
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//button[contains(.,\'Reservar\')]")), 50000)
    await driver.findElement(By.xpath("//button[contains(.,\'Reservar\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("10")
    await driver.findElement(By.xpath("//button[contains(.,\'Confirmar\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Salir\')]")).click()
  })
})

describe('Compra_Boleto_Incorrecto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Compra_Boleto_Incorrecto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/label")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Turista@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/reservarvuelo\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/reservarvuelo\')]")).click()
    {
      const element = await driver.findElement(By.xpath("//a[contains(@href, \'/reservarvuelo\')]"))
      // await driver.actions({ bridge: true }).moveToElement(element).perform()
    }
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//button[contains(.,\'Reservar\')]")), 50000)
    await driver.findElement(By.xpath("//button[contains(.,\'Reservar\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("20")
  })
})

describe('Renta_Auto_Correcto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Renta_Auto_Correcto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Turista@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/reservarauto\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/reservarauto\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//button[contains(.,\'Reservar\')]")), 50000)
    await driver.findElement(By.xpath("//button[contains(.,\'Reservar\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Alfa Romeo")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).sendKeys("2022")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).sendKeys("200")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("2022-07-05")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).sendKeys("2022-07-06")
    await driver.findElement(By.xpath("//button[contains(.,\'Confirmar\')]")).click()
  })
})

describe('Renta_Auto_Incorrecto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Renta_Auto_Incorrecto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Turista@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/reservarauto\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/reservarauto\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//button[contains(.,\'Reservar\')]")), 50000)
    await driver.findElement(By.xpath("//button[contains(.,\'Reservar\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Alfa Romeo")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).sendKeys("2022")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).sendKeys("200")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("2022-07-13")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).sendKeys("2022-07-16")
  })
})

describe('Reserva_Hotel_Correcto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Reserva_Hotel_Correcto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Turista@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/reservarhotel\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/reservarhotel\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//button[contains(.,\'Reservar\')]")), 50000)
    await driver.findElement(By.xpath("//button[contains(.,\'Reservar\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("Quezaltenango")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("10")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("1")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).sendKeys("2")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).sendKeys("200")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("2022-07-04")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).sendKeys("2022-07-07")
    await driver.findElement(By.xpath("//button[contains(.,\'Confirmar\')]")).click()
  })
})

describe('Reserva_Hotel_Incorrecto', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Reserva_Hotel_Incorrecto', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1936, height: 1048 })
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div/input")).sendKeys("Turista@gmail.com")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("fff")
    await driver.findElement(By.xpath("//button[contains(.,\'Login\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//a[contains(@href, \'/reservarhotel\')]")), 50000)
    await driver.findElement(By.xpath("//a[contains(@href, \'/reservarhotel\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.findElement(By.xpath("//button[contains(.,\'Buscar\')]")).click()
    await driver.wait(until.elementLocated(By.xpath("//button[contains(.,\'Reservar\')]")), 50000)
    await driver.findElement(By.xpath("//button[contains(.,\'Reservar\')]")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[2]/input")).sendKeys("Quezaltenango")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[3]/input")).sendKeys("2")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[4]/input")).sendKeys("200")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[5]/input")).sendKeys("2022-07-05")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).click()
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/input")).sendKeys("2022-07-01")
  })
})



describe('crear habitacion', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('crear habitacion', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1722, height: 908 })
    await driver.findElement(By.xpath("//input[@id=\'name\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'name\']")).sendKeys("hotel@gmail.com")
    await driver.findElement(By.xpath("//input[@id=\'pass\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'pass\']")).sendKeys("fff")
    await driver.findElement(By.css(".btn")).click()
    await driver.findElement(By.xpath("//a[contains(text(),\'Crear habitaci�n\')]")).click()
    await driver.findElement(By.xpath("//input[@id=\'id\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'id\']")).sendKeys("1")
    await driver.findElement(By.xpath("//input[@id=\'tipo\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'tipo\']")).sendKeys("sencilla")
    await driver.findElement(By.xpath("//input[@id=\'precio\']")).sendKeys("100")
    await driver.findElement(By.xpath("//input[@id=\'capacidad\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'capacidad\']")).sendKeys("1")
    await driver.findElement(By.xpath("//input[@id=\'descripcion\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'descripcion\']")).sendKeys("habitacion sencilla")
    await driver.findElement(By.css(".form-group:nth-child(8) > .form-control")).click()
    {
      const dropdown = await driver.findElement(By.css(".form-group:nth-child(8) > .form-control"))
      await dropdown.findElement(By.xpath("//option[. = 'Ciudad de Guatemala']")).click()
    }
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).sendKeys("0002-01-01")
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).sendKeys("0020-01-01")
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).sendKeys("0200-01-01")
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).sendKeys("2000-01-01")
    await driver.findElement(By.css(".btn-outline-primary")).click()
  })
})


describe('crear_habitacion_fail', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('crear_habitacion_fail', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1920, height: 998 })
    await driver.findElement(By.xpath("//input[@id=\'name\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'name\']")).sendKeys("hotel@gmail.com")
    await driver.findElement(By.xpath("//input[@id=\'pass\']")).sendKeys("fff")
    await driver.findElement(By.css(".btn")).click()
    await driver.findElement(By.xpath("//a[contains(text(),\'Crear habitaci�n\')]")).click()
    await driver.findElement(By.xpath("//input[@id=\'id\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'id\']")).sendKeys("aaa")
    await driver.findElement(By.xpath("//input[@id=\'tipo\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'tipo\']")).sendKeys("sencilla")
    await driver.findElement(By.xpath("//input[@id=\'precio\']")).sendKeys("aaa")
    await driver.findElement(By.xpath("//input[@id=\'capacidad\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'capacidad\']")).sendKeys("aa")
    await driver.findElement(By.xpath("//input[@id=\'descripcion\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'descripcion\']")).sendKeys("aa")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/div[6]/select")).click()
    {
      const dropdown = await driver.findElement(By.css(".form-group:nth-child(8) > .form-control"))
      await dropdown.findElement(By.xpath("//option[. = 'Ciudad de Guatemala']")).click()
    }
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).sendKeys("0002-01-01")
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).sendKeys("0020-01-01")
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).sendKeys("0200-01-01")
    await driver.findElement(By.xpath("//input[@id=\'fechaInicio\']")).sendKeys("2000-01-01")
    await driver.findElement(By.xpath("//div[@id=\'root\']/div/div/div/button")).click()
  })
})


describe('eliminar_habitacion', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('eliminar_habitacion', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1920, height: 998 })
    await driver.findElement(By.xpath("//input[@id=\'name\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'name\']")).sendKeys("hotel@gmail.com")
    await driver.findElement(By.xpath("//input[@id=\'pass\']")).sendKeys("fff")
    await driver.findElement(By.css(".btn")).click()
    {
      const element = await driver.findElement(By.css(".btn-outline-primary"))
      await driver.actions({ bridge: true }).moveToElement(element).perform()
    }
    await driver.findElement(By.xpath("//a[contains(text(),\'Eliminar habitaci�n\')]")).click()
    await driver.findElement(By.xpath("//input[@id=\'id\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'id\']")).sendKeys("1")
    await driver.findElement(By.css(".btn-outline-primary")).click()
  })
})



describe('eliminar_habitacion_fail', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('eliminar_habitacion_fail', async function() {
    await driver.get("http://34.168.113.1:3000/homeadmin")
    await driver.manage().window().setRect({ width: 1920, height: 998 })
    await driver.findElement(By.xpath("//a[contains(text(),\'Eliminar habitaci�n\')]")).click()
    await driver.findElement(By.xpath("//input[@id=\'id\']")).click()
    await driver.findElement(By.xpath("//input[@id=\'id\']")).sendKeys("aa")
    await driver.findElement(By.css(".btn-outline-primary")).click()
  })
})


describe('Buscar_habitacion_good', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Buscar_habitacion', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1920, height: 998 })
    await driver.findElement(By.id("name")).click()
    await driver.findElement(By.id("name")).sendKeys("hotel@gmail.com")
    await driver.findElement(By.id("pass")).sendKeys("fff")
    await driver.findElement(By.css(".btn")).click()
    {
      const element = await driver.findElement(By.css(".btn-outline-primary"))
      await driver.actions({ bridge: true }).moveToElement(element).perform()
    }
    await driver.findElement(By.linkText("Consultar reservas")).click()
    await driver.findElement(By.name("fechainicio")).click()
    await driver.findElement(By.name("fechainicio")).sendKeys("0002-01-01")
    await driver.findElement(By.name("fechainicio")).sendKeys("0020-01-01")
    await driver.findElement(By.name("fechainicio")).sendKeys("0200-01-01")
    await driver.findElement(By.name("fechainicio")).sendKeys("2000-01-01")
    await driver.findElement(By.css(".m-1")).click()
  })
})

describe('Buscar_habitacion_fail', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Buscar_habitacion_fail', async function() {
    await driver.get("http://34.168.113.1:3000/login")
    await driver.manage().window().setRect({ width: 1920, height: 998 })
    await driver.findElement(By.id("name")).click()
    await driver.findElement(By.id("name")).sendKeys("hotel@gmail.com")
    await driver.findElement(By.id("pass")).sendKeys("fff")
    await driver.findElement(By.css(".btn")).click()
    {
      const element = await driver.findElement(By.css(".btn-outline-primary"))
      await driver.actions({ bridge: true }).moveToElement(element).perform()
    }
    await driver.findElement(By.linkText("Consultar reservas")).click()
    await driver.findElement(By.name("fechainicio")).click()
    await driver.findElement(By.name("fechainicio")).sendKeys("aaa")
    await driver.findElement(By.name("fechainicio")).sendKeys("0020-01-01")
    await driver.findElement(By.name("fechainicio")).sendKeys("0200-01-01")
    await driver.findElement(By.name("fechainicio")).sendKeys("2000-01-01")
    await driver.findElement(By.css(".m-1")).click()
  })
})