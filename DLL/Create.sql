DROP TABLE IF EXISTS RentaAuto;
DROP TABLE IF EXISTS ReservaHotel;
DROP TABLE IF EXISTS ReservaVuelo;
DROP TABLE IF EXISTS Auto;
DROP TABLE IF EXISTS Marca;
DROP TABLE IF EXISTS Vuelo;
DROP TABLE IF EXISTS Habitacion;
DROP TABLE IF EXISTS Resena;
DROP TABLE IF EXISTS Servicio;
DROP TABLE IF EXISTS TipoServicio;
DROP TABLE IF EXISTS Usuario;
DROP TABLE IF EXISTS TipoUsuario;
DROP TABLE IF EXISTS Ciudad;
DROP TABLE IF EXISTS Pais;

CREATE TABLE Pais(
    id_pais INT AUTO_INCREMENT NOT NULL,
    pais VARCHAR(25),
    PRIMARY KEY (id_pais)
);

CREATE TABLE Ciudad(
    id_ciudad INT AUTO_INCREMENT NOT NULL,
    ciudad VARCHAR(25) NOT NULL,
    id_pais INT NOT NULL,
    PRIMARY KEY (id_ciudad),
    FOREIGN KEY (id_pais) REFERENCES Pais(id_pais)
);

CREATE TABLE TipoUsuario(
    id_tipo_usuario INT AUTO_INCREMENT NOT NULL,
    tipo_usuario VARCHAR(25) NOT NULL,
    PRIMARY KEY (id_tipo_usuario)
);

CREATE TABLE Usuario(
    id_usuario INT AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(25) NOT NULL,
    fecha VARCHAR(20) NOT NULL,
    email VARCHAR(50) NOT NULL,
    pass VARCHAR(100) NOT NULL,
    id_tipo_usuario INT NOT NULL,
    PRIMARY KEY (id_usuario),
    FOREIGN KEY (id_tipo_usuario) REFERENCES TipoUsuario(id_tipo_usuario)
);

CREATE TABLE TipoServicio(
    id_tipo_servicio INT AUTO_INCREMENT NOT NULL,
    servicio VARCHAR(25) NOT NULL,
    PRIMARY KEY (id_tipo_servicio)
);

CREATE TABLE Servicio(
    id_servicio INT AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    id_tipo_servicio INT NOT NULL,
    id_ciudad INT NOT NULL,
    PRIMARY KEY (id_servicio),
    FOREIGN KEY (id_tipo_servicio) REFERENCES TipoServicio(id_tipo_servicio),
    FOREIGN KEY (id_ciudad) REFERENCES Ciudad(id_ciudad)
);

CREATE TABLE Resena(
    id_resena INT AUTO_INCREMENT NOT NULL,
    comentario VARCHAR(200) NOT NULL,
    id_usuario INT NOT NULL,
    id_servicio INT NOT NULL,
    PRIMARY KEY (id_resena),
    FOREIGN KEY (id_usuario) REFERENCES Usuario(id_usuario),
    FOREIGN KEY (id_servicio) REFERENCES Servicio(id_servicio)

);

CREATE TABLE Habitacion(
    id_habitacion INT AUTO_INCREMENT NOT NULL,
    no_habitacion INT NOT NULL,
    tipo VARCHAR(50) NOT NULL,
    precio FLOAT NOT NULL,
    fecha VARCHAR(20) NOT NULL,
    capacidad INT NOT NULL,
    descip VARCHAR(200) NOT NULL,
    id_servicio INT NOT NULL,
    id_ciudad INT NOT NULL,
    PRIMARY KEY (id_habitacion),
    FOREIGN KEY (id_servicio) REFERENCES Servicio(id_servicio),
    FOREIGN KEY (id_ciudad) REFERENCES Ciudad(id_ciudad)
);

CREATE TABLE Vuelo(
    id_vuelo INT AUTO_INCREMENT NOT NULL,
    fecha VARCHAR(20) NOT NULL,
    asiento INT NOT NULL,
    precio FLOAT NOT NULL,
    origen INT NOT NULL,
    destino INT NOT NULL,
    vuelta INT NOT NULL,
    id_servicio INT NOT NULL,
    PRIMARY KEY (id_vuelo),
    FOREIGN KEY (origen) REFERENCES Pais(id_pais),
    FOREIGN KEY (destino) REFERENCES Pais(id_pais),
    FOREIGN KEY (id_servicio) REFERENCES Servicio(id_servicio)
);

CREATE TABLE Marca(
    id_marca INT AUTO_INCREMENT NOT NULL,
    marca VARCHAR(20) NOT NULL,
    PRIMARY KEY (id_marca)
);

CREATE TABLE Auto(
    placa VARCHAR(30) NOT NULL,
    modelo VARCHAR(25) NOT NULL,
    precio FLOAT NOT NULL,
    id_servicio INT NOT NULL,
    id_marca INT NOT NULL,
    id_ciudad INT NOT NULL,
    PRIMARY KEY (placa),
    FOREIGN KEY (id_servicio) REFERENCES Servicio(id_servicio),
    FOREIGN KEY (id_marca) REFERENCES Marca(id_marca),
    FOREIGN KEY (id_ciudad) REFERENCES Ciudad(id_ciudad)
);

CREATE TABLE ReservaHotel(
    id_reserva_hotel INT AUTO_INCREMENT NOT NULL,
    fecha_inicio VARCHAR(20) NOT NULL,
    fecha_fintipo VARCHAR(20) NOT NULL,
    id_usuario INT NOT NULL,
    id_habitacion INT NOT NULL,
    PRIMARY KEY (id_reserva_hotel),
    FOREIGN KEY (id_usuario) REFERENCES Usuario(id_usuario),
    FOREIGN KEY (id_habitacion) REFERENCES Habitacion(id_habitacion)
);

CREATE TABLE ReservaVuelo(
    id_reserva_vuelo INT AUTO_INCREMENT NOT NULL,
    asiento INT NOT NULL,
    id_usuario INT NOT NULL,
    id_vuelo INT NOT NULL,
    PRIMARY KEY (id_reserva_vuelo),
    FOREIGN KEY (id_usuario) REFERENCES Usuario(id_usuario),
    FOREIGN KEY (id_vuelo) REFERENCES Vuelo(id_vuelo)
);

CREATE TABLE RentaAuto(
    id_renta_auto INT AUTO_INCREMENT NOT NULL,
    fecha_inicio VARCHAR(20) NOT NULL,
    fecha_final VARCHAR(20) NOT NULL,
    placa VARCHAR(20) NOT NULL,
    id_usuario INT NOT NULL,
    PRIMARY KEY (id_renta_auto),
    FOREIGN KEY (id_usuario) REFERENCES Usuario(id_usuario),
    FOREIGN KEY (placa) REFERENCES Auto(placa)
);