DROP PROCEDURE IF EXISTS login;
CREATE PROCEDURE login(pEmail VARCHAR(100), pPass VARCHAR(100))
BEGIN

    IF (
        SELECT COUNT(*)
        FROM Usuario u
        WHERE u.email = pEmail
        AND u.pass = pPass
        ) <> 0
    THEN
        SELECT u.id_usuario iduser, u.nombre fullname, TU.tipo_usuario rol, '' msg
        FROM Usuario u
        INNER JOIN TipoUsuario TU on u.id_tipo_usuario = TU.id_tipo_usuario
        WHERE u.email = pEmail AND u.pass = pPass;
    ELSE
        SELECT -1 iduser, 'No existe el usuario' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS addUser;
CREATE PROCEDURE addUser
    (
    pNombre VARCHAR(25), pFecha VARCHAR(20), pEmail VARCHAR(50),
    pPass VARCHAR(50), pId_Tipo_Usuario INT
    )
BEGIN
    IF(
        SELECT COUNT(*)
        FROM Usuario u
        WHERE u.email = pEmail
        ) = 0
    THEN

        INSERT INTO Usuario (nombre, fecha, email, pass, id_tipo_usuario)
        VALUES (pNombre, pFecha, pEmail, pPass, pId_tipo_usuario);

        IF (
            pId_Tipo_Usuario <> 1
            )
        THEN
            INSERT INTO Servicio (nombre, email, id_tipo_servicio, id_ciudad)
            VALUES (pNombre, pEmail, pId_Tipo_Usuario - 1, 1);

            SELECT s.id_servicio iduser, '' msg
            FROM Servicio s
            WHERE s.nombre = pNombre
            AND s.id_tipo_servicio = pId_Tipo_Usuario;
        ELSE
            INSERT INTO Servicio (nombre, email, id_tipo_servicio, id_ciudad)
            VALUES (pNombre, pEmail, 1, 1);

            SELECT u.id_usuario iduser,'' msg
            FROM Usuario u
            WHERE u.email = pEmail;
        END IF;
    ELSE
        SELECT -1 iduser, 'Ya existe el usuario' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS addServicio;
CREATE PROCEDURE addServicio
    (
    pNombre VARCHAR(50), pEmail VARCHAR(50),
    pId_Tipo_Servicio INT, pId_Ciudad INT
    )
BEGIN

    IF (
        SELECT COUNT(*)
        FROM Servicio s
        WHERE s.nombre = pNombre
        AND s.id_tipo_servicio = pId_tipo_servicio
        ) = 0
    THEN
        INSERT INTO Servicio (nombre, email, id_tipo_servicio, id_ciudad)
        VALUES (pNombre, pEmail, pId_Tipo_Servicio, pId_Ciudad);

        SELECT s.id_servicio, '' msg
        FROM Servicio s
        WHERE s.nombre = pNombre
        AND s.id_tipo_servicio = pId_Tipo_Servicio;
    ELSE
        SELECT -1 idservicio, 'Ya existe un servicio registrado con este nombre y este tipo' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS addVuelo;
CREATE PROCEDURE addVuelo
    (
    pFecha VARCHAR(20), pOrigen VARCHAR(50), pDestino VARCHAR(50),
    pAsiento INT, pPrecio FLOAT, pVuelta INT, pServicio INT
    )
BEGIN
    DECLARE pId_origen INT;
    DECLARE pId_destino INT;

    IF(
        SELECT COUNT(*)
        FROM Pais p
        WHERE p.pais = pOrigen
        ) <> 0
    THEN
        IF(
            SELECT COUNT(*)
            FROM Pais p
            WHERE p.pais = pDestino
            ) <> 0
        THEN
            SELECT p.id_pais
            INTO pId_origen
            FROM Pais p
            WHERE p.pais = pOrigen;

            SELECT p.id_pais
            INTO pId_destino
            FROM Pais p
            WHERE p.pais = pDestino;

            INSERT INTO Vuelo(fecha, asiento, precio, origen, destino, vuelta, id_servicio)
            VALUES (pFecha, pAsiento, pPrecio, pId_origen, pId_destino, pVuelta, pServicio);

            SELECT v.id_vuelo, '' msg
            FROM Vuelo v
            ORDER BY v.id_vuelo DESC
            LIMIT 1;
        ELSE
            SELECT -1 id_vuelo, 'No existe el pais destino' msg;
        END IF;
    ELSE
        SELECT -1 id_vuelo, 'No existe el pais origen' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS addAuto;
CREATE PROCEDURE addAuto
    (
    pPlaca VARCHAR(30), pId_marca INT, pServicio INT,
    pModelo VARCHAR(20), pPrecio FLOAT, pCiudad INT
    )
BEGIN

    IF(
        SELECT COUNT(*)
        FROM Auto a
        WHERE a.placa = pPlaca
        ) = 0
    THEN
        INSERT INTO Auto(placa, modelo, precio, id_servicio, id_marca, id_ciudad)
        VALUES (pPlaca, pModelo, pPrecio, pServicio, pId_marca, pCiudad);

        SELECT pPlaca idvehiculo, '' msg;
    ELSE
        SELECT -1 idvehiculo, 'Ya existe un carro registrado con este numero de placa' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS addHabitacion;
CREATE PROCEDURE addHabitacion
    (
    pNo_habitacion INT, pTipo VARCHAR(50), pPrecio FLOAT,
    pFecha VARCHAR(20), pCapacidad INT, pDescip VARCHAR(200),
    pId_Servicio INT, pId_Ciudad INT
    )
BEGIN

    INSERT INTO Habitacion(no_habitacion, tipo, precio, fecha, capacidad, descip, id_servicio, id_ciudad)
    VALUES (pNo_habitacion, pTipo, pPrecio, pFecha, pCapacidad, pDescip, pId_Servicio, pId_Ciudad);

    SELECT h.id_habitacion idhabitacion, '' msg
    FROM Habitacion h
    ORDER BY h.id_habitacion DESC
    LIMIT 1;
END;

DROP PROCEDURE IF EXISTS addResena;
CREATE PROCEDURE addResena(pComentario VARCHAR(200), pId_User INT, pId_Servicio INT)
BEGIN
    IF (
        SELECT COUNT(*)
        FROM Usuario u
        WHERE u.id_usuario = pId_User
        ) <> 0
    THEN
        IF (
        SELECT COUNT(*)
        FROM Servicio s
        WHERE s.id_servicio = pId_Servicio
        ) <> 0
        THEN
            INSERT INTO Resena(comentario, id_usuario, id_servicio)
            VALUES (pComentario, pId_User, pId_Servicio);

            SELECT r.id_resena idresena, '' msg
            FROM Resena r
            ORDER BY r.id_resena DESC
            LIMIT 1;
        ELSE
            SELECT -1 idresena, 'No existe el servicio' msg;
        END IF;
    ELSE
        SELECT -1 idresena, 'No existe el usuario' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS addReservaHotel;
CREATE PROCEDURE addReservaHotel
    (
    pFecha_Inicio VARCHAR(20),pFecha_Fin VARCHAR(20),
    pId_User INT, pId_Habitacion INT
    )
BEGIN
    IF (
        SELECT COUNT(*)
        FROM Usuario u
        WHERE u.id_usuario = pId_User
        ) <> 0
    THEN
        IF (
        SELECT COUNT(*)
        FROM Habitacion s
        WHERE s.id_habitacion = pId_Habitacion
        ) <> 0
        THEN
            INSERT INTO ReservaHotel(fecha_inicio, fecha_fintipo, id_usuario, id_habitacion)
            VALUES (pFecha_Inicio, pFecha_Fin, pId_User, pId_Habitacion);

            SELECT rh.id_reserva_hotel idreservahotel, rh.fecha_inicio fechainicio, rh.fecha_fintipo fechafin, '' msg
            FROM ReservaHotel rh
            ORDER BY rh.id_reserva_hotel DESC
            LIMIT 1;
        ELSE
            SELECT -1 idreservahotel, 'No existe la habitacion' msg;
        END IF;
    ELSE
        SELECT -1 idreservahotel, 'No existe el usuario' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS addReservaVuelo;
CREATE PROCEDURE addReservaVuelo(pAsiento INT, pId_User INT, pId_Vuelo INT)
BEGIN
    IF (
        SELECT COUNT(*)
        FROM Usuario u
        WHERE u.id_usuario = pId_User
        ) <> 0
    THEN
        IF (
        SELECT COUNT(*)
        FROM Vuelo s
        WHERE s.id_vuelo = pId_Vuelo
        ) <> 0
        THEN
            INSERT INTO ReservaVuelo(asiento, id_usuario, id_vuelo)
            VALUES (pAsiento, pId_User, pId_Vuelo);

            UPDATE Vuelo v SET v.asiento = v.asiento - pAsiento
            WHERE v.id_vuelo = pId_Vuelo;

            SELECT rv.id_reserva_vuelo idreservavuelo, '' msg
            FROM ReservaVuelo rv
            ORDER BY rv.id_reserva_vuelo DESC
            LIMIT 1;
        ELSE
            SELECT -1 idreservavuelo, 'No existe el servicio' msg;
        END IF;
    ELSE
        SELECT -1 idreservavuelo, 'No existe el usuario' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS addRentaAuto;
CREATE PROCEDURE addRentaAuto(pFecha_Inicio VARCHAR(20), pFecha_Fin VARCHAR(20), pId_User INT, pPlaca VARCHAR(20))
BEGIN
    IF (
        SELECT COUNT(*)
        FROM Usuario u
        WHERE u.id_usuario = pId_User
        ) <> 0
    THEN
        IF (
        SELECT COUNT(*)
        FROM Auto a
        WHERE a.placa = pPlaca
        ) <> 0
        THEN
            INSERT INTO RentaAuto(fecha_inicio, fecha_final, placa, id_usuario)
            VALUES (pFecha_Inicio, pFecha_Fin, pPlaca, pId_User);

            SELECT ra.id_renta_auto idrentaauto, '' msg
            FROM RentaAuto ra
            ORDER BY ra.id_renta_auto DESC
            LIMIT 1;
        ELSE
            SELECT -1 idrentaauto, 'No existe el auto' msg;
        END IF;
    ELSE
        SELECT -1 idrentaauto, 'No existe el usuario' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS getPais;
CREATE PROCEDURE getPais()
BEGIN
    SELECT p.id_pais, p.pais
    FROM Pais p;
END;

DROP PROCEDURE IF EXISTS getCiudad;
CREATE PROCEDURE getCiudad()
BEGIN
    SELECT c.id_ciudad, c.ciudad, c.id_pais
    FROM Ciudad c;
END;

DROP PROCEDURE IF EXISTS getMarca;
CREATE PROCEDURE getMarca()
BEGIN
    SELECT m.id_marca, m.marca
    FROM Marca m;
END;

DROP PROCEDURE IF EXISTS getResenaUsuario;
CREATE PROCEDURE getResenaUsuario(pId_User INT)
BEGIN
    IF(
        SELECT COUNT(*)
        FROM Resena r
        WHERE r.id_usuario = pId_User
        ) <> 0
    THEN
        SELECT r.id_resena, r.comentario, r.id_servicio, '' msg
        FROM Resena r
        WHERE r.id_usuario = pId_User;
    ELSE
        SELECT -1 id_resena, 'No existen resenas de este usuario' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS getResenaServicio;
CREATE PROCEDURE getResenaServicio(pId_Servicio INT)
BEGIN
    IF(
        SELECT COUNT(*)
        FROM Resena r
        WHERE r.id_servicio = pId_Servicio
        ) <> 0
    THEN
        SELECT r.id_resena, r.comentario, r.id_usuario, '' msg
        FROM Resena r
        WHERE r.id_servicio = pId_Servicio;
    ELSE
        SELECT -1 id_resena, 'No existen resenas de este servicio' msg;
    END IF;
END;

DROP PROCEDURE IF EXISTS getRentaTurista;
CREATE PROCEDURE getRentaTurista(pId_User INT)
BEGIN
    SELECT a.placa, a.modelo, a.precio, m.marca, c.ciudad
    FROM Auto a
    INNER JOIN RentaAuto ra ON a.placa = ra.placa
    INNER JOIN Marca m ON m.id_marca = a.id_marca
    INNER JOIN Ciudad c ON c.id_ciudad = a.id_ciudad
    WHERE ra.id_usuario = pId_User;
END;

DROP PROCEDURE IF EXISTS getRentaServicio;
CREATE PROCEDURE getRentaServicio(pId_Servicio INT)
BEGIN
    SELECT a.placa, a.modelo, a.precio, m.marca, c.ciudad
    FROM Auto a
    INNER JOIN RentaAuto ra ON a.placa = ra.placa
    INNER JOIN Marca m ON m.id_marca = a.id_marca
    INNER JOIN Ciudad c ON c.id_ciudad = a.id_ciudad
    INNER JOIN Servicio s ON s.id_servicio = a.id_servicio
    WHERE s.id_servicio = pId_Servicio;
END;

DROP PROCEDURE IF EXISTS getVuelo;
CREATE PROCEDURE getVuelo(pId_Vuelo INT)
BEGIN
    SELECT v.precio, p.pais origen, p2.pais destino
    FROM Vuelo v
    INNER JOIN Pais p ON p.id_pais = v.origen
    INNER JOIN Pais p2 ON p2.id_pais = v.destino
    WHERE v.id_vuelo = pId_Vuelo;
END;

DROP PROCEDURE IF EXISTS getResena;
CREATE PROCEDURE getResena(pId_Resena INT)
BEGIN
    IF(
        SELECT COUNT(*)
        FROM Resena r
        WHERE r.id_resena = pId_Resena
        ) <> 0
    THEN
        SELECT r.id_resena, r.comentario, r.id_usuario, r.id_servicio
        FROM Resena r
        WHERE r.id_resena = pId_Resena;
    ELSE
        SELECT -1 id_resena;
    END IF;
END;

DROP PROCEDURE IF EXISTS getUsuario;
CREATE PROCEDURE getUsuario(pId_Usuario INT)
BEGIN
    IF(
        SELECT COUNT(*)
        FROM Usuario u
        WHERE u.id_usuario = pId_Usuario
        ) <> 0
    THEN
        SELECT u.id_usuario
        FROM Usuario u
        WHERE u.id_usuario = pId_Usuario;
    ELSE
        SELECT -1 id_usuario;
    END IF;
END;