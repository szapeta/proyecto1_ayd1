# FULL TRIP 
------------------
### Integrantes
|Carne | Nombre |
|-----|-----|
| 201801329 | Mynor Rene Ruiz Guerra |
| 201806840 | Byron Rubén Hernández de León  |
| 201902934 | German Jose Paz Cordon |
| 200715274 | Sergio Mynor David Felipe Zapeta |
| 201709110 | Wilson Eduardo Perez Echeverria |

# Metodología de desarrollo

Metodología para utilizar: SCRUM

### Roles:
- Scrum Máster: Mynor Ruiz
- Product Owner: Manuel de Mata
- Scrum developer: Sergio Zapeta 
- Scrum developer: German Paz
- Scrum developer: Wilson Perez
- Scrum developer: Byron Hernandez

Se escogió esta metodología para poder conseguir un ritmo constante a lo largo del desarrollo del proyecto, tanto en duración del sprint como de esfuerzo.
Esta metodología está estructurada para que nos ayude a adaptarnos de forma natural a las condiciones cambiantes y a los requisitos del proyecto, ciclos de lanzamiento breves para que el equipo pueda aprender a generar entregables funcionales y mejorar constantemente.
Debido al escaso tiempo disponible de cada miembro del equipo, se pensó en utilizar una metodología que brindara una organización diaria y ágil como Scrum, los daily meeting se realizaron en la noche, tuvieron un tiempo promedio de 15 minutos y se comunicó las funcionalidades que se trabajó ese día y se estableció las tareas a realizar el día siguiente, para poder distribuir el trabajo de manera equitativa y poder trabajar por separado en el transcurso del día.
Durante el daily scrum, cada miembro del equipo responde las siguientes 3 preguntas:
1.	¿Qué hiciste hoy?
2.	¿Que harás mañana?
3.	¿Hay impedimentos en tus tareas?

Uno de los beneficios que encontramos en esta metodología fue que, al solicitar revisión de las tareas realizadas, se obtuvo una retroalimentación y pudimos organizar mejor esos cambios en el día a día, otra ventaja que se percibió fue que al juntarnos todos los días tuvimos mayor conciencia del tiempo requerido para realizar el proyecto y se pudieron tomar ciertas decisiones según el tiempo restante.
Una de las desventajas que tuvimos fue que la probabilidad que uno de los estudiantes no estuviera disponible para el daily meeting era bastante alta, ya que se realizaba todos los días y era probable que algunos integrantes les pasara algunos imprevistos. 


----

## Modelo de branching
Se utilizo este flujo para tener un mejor control de código y que la implementación sea mas rápida. Los cambios fueron subidos desde la rama feature por todos los desarrolladores para evitar subir funcionalidades con problemas. 
Cuando se sube funcionalidades es necesario realizar un merge hacia la rama “develop” y posteriormente a la rama “main”, creando su reléase correspondiente. Se decidió trabajar de esta forma ya que algunos desarrolladores trabajan sobre el mismo archivo y algunas funcionalidades dependen de otras funcionalidades, entonces esto nos beneficia para poder versionar mejor el código y verificar el buen desarrollo de las funcionalidades, si se tuviera algún error en una funcionalidad en específico, se podría localizar fácilmente por medio de los reléase.
La desventaja de este flujo es que algunas funcionalidades dependen de otras, quiere decir que algunos desarrolladores no podrán comenzar a trabajar hasta que algún desarrollador complete la funcionalidad que requieren. 
Todos los desarrolladores trabajaran en todas las ramas, ya que se distribuyo de manera equitativa todas las partes del proyecto: documentación, funcionalidades del backend y funcionalidades del frontend.
Se creo el repositorio con la rama “main”, posteriormente se creó otra rama llamada “develop”. 
Para la documentación se agrego una nueva rama “feature”, se creó una carpeta dentro llamada “Documentacion” y luego se agregó la documentación por cada uno de los desarrolladores, se realizó un merge a la rama “Develop” y posteriormente se hizo merge a la rama “main”.
Se creo el reléase “1.1.0” para la documentación y su tag correspondiente.
Se asigno a cada desarrollador una funcionalidad a realizar, para subir estas funcionalidades se utilizó la rama “feature” donde se separó en carpetas los cambios realizados por cada desarrollador. 
Posteriormente por cada desarrollador se hizo un merge a la rama “develop”, luego se hizo merge de la rama “develop” a “main”, se creó un release y se creó su respectivo tag.
Para el frontend se agregó a la rama “feature”, se creó una carpeta dentro llamada “Frontend” y luego se agregó la parte del Frontend correspondiente de cada desarrollador, se realizó un merge a la rama “Develop” y posteriormente se hizo merge a la rama “main”.
Se creo el reléase “2.6.0” para la documentación y su tag correspondiente.

![](img/modelo%20branching.png)

----
## Requerimientos
### Analisis y Recoleccion de Datos 
Durante el analisis para la toma de requerimientos logramos identificar que las personas claves en el sistema seran los administradores y los usuarios turista , estos don son importantes ya que mientras unos proveen el servicio otros obtienen ese servicio, como obstaculo principal tenemos lo amistoso que puede ser el software a desarrollar , por lo tanto se han observado los lineamientos de desempeño de cada usuario en un ambiente controlado , obteniendo como resultado que en la mayoria de ocasiones el usuario quiere obtener filtros en cualquier busqueda que desee realizar y que los administradores de los servicios siempre quieren llegar a un tipo de clientes determinados, asi mismo nos hemos percatado que las areas que requieren de un control especifico son las de registro de todos los servicios ya que estos requieren de una guia mas especializada.
### Funcionales
#### Generales 
* REQ 1: Registro de Usuarios 
* REQ 10: Gestion de cuenta de perfil usuario
* REQ 11: Inicio de sesión 
* REQ 15: Ver reseñas 
* REQ 16: Actualizar Datos 
#### Administrador 
* REQ 2: Registro de vehículos 
* REQ 3: Registro Aviones 
* REQ 4: Registro de Hoteles 
* REQ 5: Registro de Vuelos
* REQ 6: Registro de habitaciones disponibles 
#### Usuarios Turistas 
* REQ 7: Implementación de búsqueda de busqueda de autos con filtros 
* REQ 8: Implementación de búsqueda de vuelos con filtros 
* REQ 9: Implementación de búsqueda de habitaciones con filtros 
* REQ 12: Calificar servicio 
* REQ 13: Comprar vuelos 
* REQ 14: Alquilar Autos 
* REQ 17: Historial de servicios 
### Usuarios Hoteles 
* REQ 18: Ver habitaciones
* REQ 19: Cambiar Tarifas 
* REQ 20: Agregados en habitacion
* REQ 21: Cargos extra de habitacion 
### Usuarios Autos 
* REQ 22: Ver autos 
* REQ 23: Cambiar precios 
* REQ 24: Agregar Promocion
* REQ 25: Ver mis ganancias 
* REQ 26: ver ubicacion Autos 
### Usuarios Aviones 
* REQ 27: Ver Vuelos
* REQ 28: Reprogramar vuelo
* REQ 29: ver Taquillas 
* REQ 30: Ver Estadisticas 
### Matriz de Servicios 
|CLAVE|"PRIORIDAD (BAJA,MEDIA,ALTA)"|
|---|---|
|REQ 1|ALTA|
|REQ 2|ALTA|
|REQ 3|ALTA|
|REQ 4|ALTA|
|REQ 5|ALTA|
|REQ 6|ALTA|
|REQ 7|MEDIA|
|REQ 8|MEDIA|
|REQ 9|MEDIA|
|REQ 10|ALTA|
|REQ 11|ALTA|
|REQ 12|MEDIA|
|REQ 13|ALTA |
|REQ 14|ALTA |
|REQ 15|ALTA |
|REQ 16|ALTA |
|REQ 17|ALTA |
|REQ 18|ALTA |
|REQ 19|ALTA |
|REQ 20|ALTA |
|REQ 21|MEDIA |
|REQ 22|ALTA |
|REQ 23|ALTA |
|REQ 24|MEDIA |
|REQ 25|MEDIA|
|REQ 26|ALTA |
|REQ 27|ALTA |
|REQ 28|ALTA |
|REQ 29|ALTA |
|REQ 30|ALTA |
### Matriz de trazabilidad 

|Requerimientos\stakeholder|administradores|Usuarios Turistas |Usuarios Hoteles |Usuarios Autos |Usuarios Aviones |
|---|---|---|---|---|---|
|REQ 1|x|||||
|REQ 2|x|||||
|REQ 3|x|||||
|REQ 4|x|||||
|REQ 5|x|x||||
|REQ 6|x|x||||
|REQ 7||x||||
|REQ 8||x||||
|REQ 9||x||||
|REQ 10||x||||
|REQ 11||x||||
|REQ 12||x||||
|REQ 13||x||||
|REQ 14||x||||
|REQ 15|x|x|x|x|x|
|REQ 16|x|x|x|x|x|
|REQ 17||x||||
|REQ 18|||x|||
|REQ 19|||x|||
|REQ 20|||x|||
|REQ 21|||x|||
|REQ 22||||x||
|REQ 23||||x||
|REQ 24||||x||
|REQ 25||||x||
|REQ 26||||x||
|REQ 27|||||x|
|REQ 28|||||x|
|REQ 29|||||x|
|REQ 30|||||x|

### No Funcionales 
#### Eficiencia 
* Toda la funcionalidad del sistema de transaccion de negocio debe responder al suaurio en menos de 5 segundos 
* El sistema debe ser capaz de operar adecuadamente con hasta 100000 usuarios con sesiones concurrentes.
* Los datos modificados en la base de datos deben ser actualizadas para todos los usarios que acceden en menos de 2 segundos.
#### Seguridad lógica y de datos 
* Todos los sistemas deben respaldarse cada 24 horas. Los respaldos deben ser almacenados en una localidad segura ubicada en un edificio distinto al que reside el sistema.
* Todas las comunicaciones externas entre servidores de datos, aplicación y cliente del sistema deben estar encriptadas utilizando el algoritmo RSA.
* Si se identifican ataques de seguridad o brecha del sistema, el mismo no continuará operando hasta ser desbloqueado por un administrador de seguridad.
#### usabilidad 
* El tiempo de aprendizaje del sistema por un usuario deberá ser menor a 4 horas.
* El sistema debe contar con manuales de usuario estructurados adecuadamente.
* El sistema debe proporcionar mensajes de error que sean informativos y orientados a usuario final.

----
## Mapeo de Historias de Usuario

![](img/Mapeo.jpg)

----
## Criterios de Aceptación
|Historia de Usuario|Rol|Funcionalidad|Resultado|Criterio de aceptacion|
|---|---|---|---|---|
|Usuario Administrador|Como dueño del negocio|quiero hacer login con un usuario que tenda todas las funcionalidades asignadas|para administrar el portal de servicios|Se necesita hacer login y desplegar todos los menus de las funcionalidades con las que cuenta el sistema. Se necesita que no tenga ninguna restriccion en las consultas|
|Usuario Administrador|Como administrador del sistema|Quiero poder iniciar sesion dentro del sistema|para poder visualizar los servicios activos con que cuenta la pagina sin|Dado un nombre de usuario y password correspondiente al usuario| ingresarlos dentro de los campos definidos y validarlos contra los almacenados en base de datos para identificar si tienen relacion entre ellos. El password debe estar almacenado en base de datos de forma encriptada|
|Usuarios hoteles|Como Administrador del sistema|Quiero registrar y guardar en BDD a los responsables de los hoteles|para que se puedan tener una cuenta activa dentro del sistema|Con un nombre de usuario y una contraseña| que puedan iniciar sesion dentro del sistema junto al rol de hotel| para que puedan visualizar y publicar servicios que ellos ofrezcan|
|Usuarios hoteles|Como dueño del negocio|Quiero consultar a los responsables de los hoteles que se encuentran guardados en base de datos|para tener una lista de los servicios ofrecidos dentro del sistema|Visualizar la totalidad de hoteles disponibles dentro del portal de servicios para que puedan publicar los servicios que ofrecen|
|Usuarios turistas|Como Administrador del sistema|Quiero registrar y guardar en BDD a los potenciales clientes que tenga el portal|para que se puedan tener una cuenta activa dentro del sistema|Con un nombre de usuario y una contraseña| que puedan iniciar sesion dentro del sistema junto al rol de de turista| para que puedan visualizar la totalidad servicios hoteles| autos y vuelos|
|Usuarios turistas|Como dueño del negocio|Quiero consultar a los clientes que se encuentran guardados en base de datos|para identificar y segmentar sus necesidades|Visualizar la totalidad de servicios hoteles| autos y vuelos disponibles dentro del portal de servicios para que puedan publicar los servicios que ofrecen|
|Usuarios Aerolineas|Como Administrador del sistema|Quiero registrar y guardar en BDD a los responsables de las aereolinas|para que se puedan tener una cuenta activa dentro del sistema|Con un nombre de usuario y una contraseña| que puedan iniciar sesion dentro del sistema junto al rol de vuelos| para que puedan visualizar y publicar servicios de vuelos que ellos ofrezcan y que esten disponibles|
|Usuarios Aerolineas|Como dueño del negocio|Quiero consultar a los responsables de las aereolinas que se encuentran guardados en base de datos|para tener una lista de los servicios ofrecidos dentro del sistema|Visualizar en una ventana| los vuelos disponibles dentro del portal de servicios para que puedan publicar los servicios que ofrecen|
|Iniciar Sesion|usuario del sistema|Quisiera iniciar sesion en la plataforma para poder consultar mi perfil|ingresar al sistema y mostrar informacion|El usuario debera confrontarse contra la base de datos para identificar al usuario que esta ingresando al sistema y asi permitirle el ingreso|
|Iniciar Sesion|Como Administrador del sistema|controlar el ingreso a la plataforma|poder controlar el inicio de sesion de los usuarios|Identificar a los usuario y restringir el acceso si fuera necesario debido a alguna falta que se identifique|
|Búsqueda de autos|Usuario del vehiculo|Realizar una búsqueda sobre los vehículos existentes.|Verificar el estado de los vehículos registrados para revisar si estan en uso y de estarlo validar los dias de uso que le quedan.|Visualizar un tabla con todos los vehículos en existencia y que se pueda ver la clasificación según su estado y si esta en uso poder revisar los días por los que fue rentado.|
|Registro de autos|Usuario del vehiculo|Registrar un nuevo auto a la base de datos del sistema.|Agregar otro vehículo para que pueda ser rentado.|Visualizar que el vehículo agregado pueda ser búscado con todos los posibles filtros y pueda ser rentado por un turista.|
|Calificar servicio|Turista del sistema|Agregar un comentario sobre la opinión personal sobre la calidad del servicio.|Agregar un comentario sobre la opinión personal sobre la calidad del servicio.|Seleccionar el tipo de servicio y poder escribir en un cuadro de texto el comentario sobre la calidad del servicio que fue utilizado.|
|Calificar servicio|Usuario del vehiculo|Revisar los comentarios que los turistan dejan sobre el servicio.|Determinar si la calidad del servicio cumple con las expectativas del usuario.|Poder generar un reporte con todos los comentarios que han hecho los turistas sobre la calidad del servicio de renta de autos.|
|Filtrar por marca|Turista del sistema|Realizar un búsquieda sobre los vehículos disponibles de una marca en especifico.|Obtener una lista con todos los vehiculos que estan disponibles de la marca dada.|Visualizar una table con los vehículos que actual mente pueden ser rentados y que pertenecen a la marca selecccionada.|
|Filtrar por marca|Usuario del vehiculo|Verificar que todos los vehículos obtenidos pertenezcan a la marca indicada.|Obtener una lista con todos los vehiculos que estan disponibles de la marca dada.|Visualizar una table con los vehículos que actual mente pueden ser rentados y que pertenecen a la marca selecccionada.|
|Filtrar por precio|Turista del sistema|Realizar un búsquieda sobre los vehículos disponibles dentro de un rango de precio.|Obtener una lista con todos los vehiculos que estan disponibles dentro del rango de precio dado.|Visualizar una table con los vehículos que actual mente pueden ser rentados y que se encuentran bajo el rango de precio seleccionado.|
|Filtrar por precio|Usuario del vehiculo|Verificar que todos los vehículos obtenidos se encuentren bajo el rango de precio indicado.|Obtener una lista con todos los vehiculos que estan disponibles dentro del rango de precio dado.|Visualizar una table con los vehículos que actual mente pueden ser rentados y que se encuentran bajo el rango de precio seleccionado.|
|Alquiler de autos|Como arrendador de autos|Quiero registrar los vehículos y sus caracteristicas|Rentar un auto|Guardado en Base de datos|
|Filtrar por modelo|Como Turista del sistema|Quiero filtrar los vehículos por sus caracteristicas|Se muestra los resultados de la busqueda|Desplegar una tabla con los resultados|
|Registro aerolinea|Como agencia de viajes|Quiero poder registrar la aerolínea encargada de brindar los servicios|Se crea una cuenta para la aerolinea|Mostrar un sistema para gestionar los vuelos|
|Registro vuelo|Como agencia de viajes|Quiero poder registrar vuelos disponibles de la aerolinea|Agregar un vuelo|Desplegar una tabla en tiempo real con los vuelos disponibles|
|Buscar Vuelo|Como Turista del sistema|Quiero ingresar propiedades de vuelo|para realizar una reserva|Desplegar una tabla con los resultados mostrando los datos existentes y de validacion|
|Filtrar por estilo|Como Turista del sistema|Quiero filtar las propiedades para una mejor comodidad|para realizar una reserva|Desplegar una tabla con los resultados mostrando los datos existentes y de validacion|
|Alquiler vuelo|Como Turista del sistema|Quiero reservar mi boleto de vuelo|para que me confirmen mi reserva|Desplegar una tablaa con los resultados de la reserva| mostando mi reserva aprovada|
|filtrar precio|Como Turista del sistema|Quiero conocer los precios de buelo mas economicos o bien de mejor calidad|Para realizar una reserva|Desplegar una tabla con los resultados|
|Buscar Vuelo|Como administrador del sistema|Quiero realizar el chequeo de las busquedas de vuelo|para tener control sobre los aerolineas disponibles|Desplegar una tabla con los resultados mostrando los datos existentes actualizados|
|Filtrar por estilo|Como administrador del sistema|Quiero filtar las propiedades para una mejor comodidad|para realizar una reserva mostando los mejores controles|Desplegar una tabla con los resultados mostrando los datos existentes y de validacion|
|Alquiler vuelo|Como administrador del sistema|Quiero configurar la reserva de voletos para mejorar el negocio|para que me confirmen las transacciones teniendo un mejor control|Desplegar una tablaa con los resultados mostrandolo los datos de banco y transacciones del negocio|
|filtrar precio|Como administrador del sistema|Quiero mejorar la calidad de los precios|para ver un mejor alquiler de aerolinea|Desplegar una tabla con los resultados con precios disponibles dando mejora|
|Registro Hoteles|Como usuario Hotel|Quiero registrar a mi hotel y sus caracteristicas|para poder utilizar los servicios de la plataforma|Guardado en base de datos|
|Registro de Habitaciones|como usuario de Hotel|Quiero registrar las habitaciones disponibles y con todo detalle|para poder estar en la tabla de busqueda que es mostrado a los turistas|Guardado en base de datos|
|Registro de reservaciones|como usuario de hotel|Quiero registrar una reservacion para un cliente en especifico|Para realizar una reservacion del hotel y obtener el codigo de reservacion|Visualizar una table con las habitaciones que actualmente pueden ser reservadas y mostrar una notificacion con el identificador de la reservacion|
|Registro de reservaciones|como usuario turista|Quiero registrar una reservacion|Para realizar una reservacion del hotel y obtener el codigo de reservacion|Visualizar una table con las habitaciones que actualmente pueden ser reservadas y mostrar una notificacion con el identificador de la reservacion|
|Buscador con Filtros|como usuario de hotel|Quiero establecer etiquetas para poder dirigirme a un publico en especifico|Para obtener una lista con todas las habitaciones que pueden ser reservadas con los criterios que he seleccionado|Poder tener una etiqueta la cual nos pueda diferenciar en el buscado cuando el turista busque un concepto en especifico|
|Buscador con Filtros|como usuario turista|Quiero buscar hoteles y habitaciones con las caracteristicas que sean de mi agrado|obtener una lista con todas las habitaciones que pueden ser reservadas con los criterios que he seleccionado|Visualizar una table con las habitaciones que actualmente pueden ser reservadas y que pertenecen a la etiqueta selecccionada.|


----

## Tecnologias 


### React
Es una librería open source de JavaScript para desarrollar interfaces de usuario debido a que tiene mas universalidad que angular al momento de realizar una web móvil y minimalista sin inyección de dependencias , no como angular que es un libreria enorme y contiene muchas sintaxis innecesarias, y como comparación final el rendimiento de react es mejor debido a la incorporación del DOM virtual, por todas estas razones se ha elegido react sobre angular.
### Flask
Es un framework que permite desarrollar aplicaciones web de forma sencilla, está especialmente guiado para un desarrollo web fácil y rápido con Python. Una de sus características a destacar es el potencial de instalar extensiones o complementos acorde al tipo de proyecto que desarrollarás. Se escogió flask sobre django debido al desempeño de velocidad en flask, ya que la velocidad es una parte muy importante en nuestro proyecto a desarrollar, debido a esto a pesar de que django representa un manejo de GET y POST más fácil debido que genera mejores REST. Las principales ventajas de flask son los micro frameworks, el servidor web de desarrollo , el depurado , la compatibilidad de WSGI, el buen manejo de rutas y que no tiene ORMs, por estas ventajas a incluyendo su mayor velocidad sobre django fue que se escogió la tecnología Flask.

### TRELLO 
Trello es un software de administración de proyectos con interfaz web y con cliente para iOS y android para organizar proyectos
### DRAW.IO
diagrams.net (anteriormente draw.io ) es un software de dibujo de gráficos multiplataforma gratuito y de código abierto desarrollado en HTML5 y JavaScript .Su interfaz se puede utilizar para crear diagramas como diagramas de flujo , estructuras alámbricas , diagramas UML , organigramas y diagramas de red 
### JAVASCRIPT 
menudo abreviado JS , es un lenguaje de programación que es una de las tecnologías centrales de la World Wide Web , junto con HTML y CSS . [11] A partir de 2022, el 98 % de los sitios web usan JavaScript en el lado del cliente para el comportamiento de la página web , [12] a menudo incorporan bibliotecas de terceros . [13]Todos los principales navegadores web tienen un motor de JavaScript dedicado para ejecutar el código en los dispositivos de los usuarios .
### NODE 
Ideado como un entorno de ejecución de JavaScript orientado a eventos asíncronos, Node.js está diseñado para crear aplicaciones network escalables. En el siguiente ejemplo de "hola mundo", pueden atenderse muchas conexiones simultáneamente. Por cada conexión, se activa la devolución de llamada o callback, pero si no hay trabajo que hacer, Node.js se dormirá.
### MEET 
oogle Meet funciona en cualquier dispositivo. Únete a reuniones desde un ordenador de escritorio o portátil, con un dispositivoAndroid o con un iPhone o iPad. Si tu organización necesita utilizar una sala de conferencias, el hardware de Google Meet ofrece opciones de gran calidad a un precio asequible.
### EXPRESS
Express.js o simplemente Express, es un entorno de trabajo para aplicaciones web para el programario Node.js, de código abierto y con licencia MIT. Se utiliza para desarrollar aplicaciones web y APIs
### CORS
CORS (Cross-Origin Resource Sharing) es un mecanismo o política de seguridad que permite controlar las peticiones HTTP asíncronas que se pueden realizar desde un navegador a un servidor con un dominio diferente de la página cargada originalmente. Este tipo de peticiones se llaman peticiones de origen cruzado (cross-origin).
### SUPERTEST JES
SuperTest is a Node. js library that helps developers test APIs. It extends another library called superagent, a JavaScript HTTP client for Node. js and the browser. Developers can use SuperTest as a standalone library or with JavaScript testing frameworks like Mocha or Jest.
### VISUAL STUDIO CODE 
Visual Studio Code es un editor de código fuente desarrollado por Microsoft para Windows, Linux, macOS y Web. Incluye soporte para la depuración, control integrado de Git, resaltado de sintaxis, finalización inteligente de código, fragmentos y refactorización de código.
### MYSQL 
es un sistema de gestión de bases de datos relacional desarrollado bajo licencia dual: Licencia pública general/Licencia comercial por Oracle Corporation y está considerada como la base de datos de código abierto más popular del mundo,​ y una de las más populares en general junto a Oracle y Microsoft SQL Server, todo para entornos de desarrollo web. 
### AMAZON RDS 
Amazon Relational Database Service es un servicio de base de datos relacional distribuida de Amazon Web Services. Es un servicio web que se ejecuta "en la nube" diseñado para simplificar la configuración, el funcionamiento y el escalado de una base de datos relacional para su uso en aplicaciones.
### DOCKER
Docker es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos.​ 
### DOCKER HUB 
Docker hub es un repositorio público en la nube, similar a Github, para distribuir los contenidos. Está mantenido por la propia Docker y hay multitud de imágenes, de carácter gratuito, que se pueden descargar y asi no tener que hacer el trabajo desde cero al poder aprovechar “plantillas”
### GITLAB 
Gitlab Inc. es una compañía de núcleo abierto y es la principal proveedora del software GitLab, un servicio web de forja, control de versiones y DevOps basado en Git.
### GITKRAKEN
GitKraken es una herramienta multi plataforma (Mac, Windows y Linux) que nos ayuda a manejar Git de manera sencilla, lo cual incide en nuestra productividad. Con GitKraken, Git realmente hace sentido, podemos abrir fácilmente repositorios, organizar favoritos y organizar estos en grupos de proyectos entre otras cosas.
### MD5
En primer lugar, debemos comprender que no existe una forma nativa de descifrar códigos hash MD5 en JavaScript. Dado que el hash MD5 es un algoritmo unidireccional, en teoría no es posible revertir los hash MD5. Hay un par de métodos alternativos que puedes usar para descifrarlos, pero no son infalibles, por lo que no hay garantía de que siempre funcionen.


----

## Diagrama de componentes

![](img/diacom.png)
----

## Diagrama de despliege

![](img/diades.png)

----

## Diagrama de Datos

![](img/Relational.jpg)

----



## Seguridad

La seguridad de identidad sera basada mediante el uso de notificaciones al correo registrado para el envío de mensajes para la confirmacion de la pertenencia al correo electronico.

----

## Mouckups de la aplicacion

### Area Administrativa

![](img/mk001.jpg)

![](img/mk002.jpg)

![](img/mk003.jpg)

![](img/mk004.jpg)

![](img/mk005.jpg)

### Area Hoteles

![](img/mk006.jpg)

![](img/mk007.jpg)

![](img/mk008.jpg)

![](img/mk009.jpg)

### Area Turistas

![](img/mk010.jpg)

![](img/mk011.jpg)

![](img/mk012.jpg)

### Area Vehiculos

![](img/mk013.jpg)

![](img/mk014.jpg)

![](img/mk015.jpg)

### Area Vuelos

![](img/mk016.jpg)

![](img/mk017.jpg)

![](img/mk018.jpg)

----

## Diagrama de Clases

![](img/Clases.jpg)

----

## Casos de uso

![](img/Casos.jpg)

----

## Herramienta para manejo de tareas

![](img/trelloini.jpg)

![](img/trellocom.jpg)

